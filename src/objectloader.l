/****************************************************************************
**
** Copyright (c) 2020 SoftAtHome
** 
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
** 
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
** 
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
** 
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
** 
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
** 
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
** 
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
** 
** DISCLAIMER
** 
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

%{
#include <stdlib.h>

#include <debug/sahtrace.h>

#include <pcb/core/object.h>
#include <pcb/core/parameter.h>
#include <pcb/core/validator.h>
#include <pcb/core/datamodel.h>
#include <pcb/core/acl.h>
#include <pcb/utils/string_list.h>

#include "objectloader.h"
#include "objectloader.tab.h"

extern string_t description;
extern string_t description_line;

void yy_state_set(int new_state) {
    BEGIN(new_state);
}

static void start_description();
static void string_dupChar(string_t **dest, const char *src);

static inline void trim_description() {
#ifdef PCB_HELP_SUPPORT
    string_trim(&description);
#endif
}

static inline void add_description(const char *text) {
#ifdef PCB_HELP_SUPPORT
    string_fromChar(&description_line, text);
    string_trim(&description_line);
    if (!string_isEmpty(&description_line)) {
      string_append(&description, &description_line);
    }
#else
  (void)text;
#endif
}

static void end_description() {
#ifdef PCB_HELP_SUPPORT
    string_appendChar(&description,"\\n");
#endif
}

extern bool functionArgs;

%}

%option stack

%x COMMENT DESCRIPTION LINECOMMENT LONGTEXT

NL  \r?\n

%%

"\"\""              {
                      odl_yylval.text = NULL;
                      return TEXT;
                    }


"/*"                { yy_push_state( COMMENT ); }

"/**"               { start_description(); }

"//"                { yy_push_state( LINECOMMENT ); }

"\""                { 
                       odl_yylval.text = calloc(1, sizeof(string_t));
                       string_initialize(odl_yylval.text, INITIAL_TEXT_SIZE);
                       yy_push_state( LONGTEXT ); 
                    }

[\n]                { yylineno++;}

[ \t]

<COMMENT>{
      "*/"          yy_pop_state();
      "*"
      [^*\n]+
      [^*\n]*{NL}   { yylineno++;}
}

<DESCRIPTION>{
      "*/"          {
                      trim_description();
                      yy_pop_state();
                    }
      "*"
      [^*\n]+       {
                      add_description(yytext);
                    }
      {NL}          { yylineno++;
                      end_description();
                    }
}

<LINECOMMENT>{
      {NL}          { 
                      yylineno++; 
                      yy_pop_state(); 
                    }
      "*"
      [^*\n]+
}

<LONGTEXT>{
      {NL}           {
                       yy_pop_state();
                     }
      [^\\"]+        { string_appendChar(odl_yylval.text, yytext); }
      [\\].          { string_appendChar(odl_yylval.text, yytext); }
      "\""           { yy_pop_state(); return TEXT; }
}

<<EOF>> {
          FILE *prev = yyin;
          odl_yypop_buffer_state();
          if (!YY_CURRENT_BUFFER) {
              yyterminate();
          } else {
              fclose(prev);
          }
          return FILE_DONE;
        }

include                 { if (functionArgs) {
                            string_dupChar(&odl_yylval.text, yytext);
                            return WORD;
                          } else {
                            odl_yylval.keyword = keyword_include;
                            return INCLUDE;
                          }
                        }

define                  { if (functionArgs) {
                            string_dupChar(&odl_yylval.text, yytext);
                            return WORD;
                          } else {
                            odl_yylval.keyword = keyword_define;
                            return DEFINE;
                          }
                        }

mib                     {  if (functionArgs) {
                            string_dupChar(&odl_yylval.text, yytext);
                            return WORD;
                          } else {
                            odl_yylval.keyword = keyword_mib;
                            return MIB;
                          }
                        }

object                  {  if (functionArgs) {
                            string_dupChar(&odl_yylval.text, yytext);
                            return WORD;
                          } else {
                            odl_yylval.keyword = keyword_object;
                            return OBJECT;
                          }
                        }

extends                 { if (functionArgs) {
                            string_dupChar(&odl_yylval.text, yytext);
                            return WORD;
                          } else {
                            odl_yylval.keyword = keyword_extends;
                            return EXTENDS;
                          }
                        }

extend                  { if (functionArgs) {
                            string_dupChar(&odl_yylval.text, yytext);
                            return WORD;
                          } else {
                            odl_yylval.keyword = keyword_extend;
                            return EXTEND;
                          }
                        }

acl                     {  if (functionArgs) {
                            string_dupChar(&odl_yylval.text, yytext);
                            return WORD;
                          } else {
                            odl_yylval.keyword = keyword_acl;
                            return ACL;
                          }
                        }

user                    { if (functionArgs) {
                            string_dupChar(&odl_yylval.text, yytext);
                            return WORD;
                          } else {
                            odl_yylval.keyword = keyword_user;
                            return USER;
                          }
                        }

group                   { if (functionArgs) {
                            string_dupChar(&odl_yylval.text, yytext);
                            return WORD;
                          } else {
                            odl_yylval.keyword = keyword_group;
                            return GROUP;
                          }
                        }

world                   { if (functionArgs) {
                            string_dupChar(&odl_yylval.text, yytext);
                            return WORD;
                          } else {
                            odl_yylval.keyword = keyword_world;
                            return WORLD;
                          }
                        }

add                     { if (functionArgs) {
                            string_dupChar(&odl_yylval.text, yytext);
                            return WORD;
                          } else {
                            odl_yylval.keyword = keyword_add;
                            return ADD;
                          }
                        }

delete                  { if (functionArgs) {
                            string_dupChar(&odl_yylval.text, yytext);
                            return WORD;
                          } else {
                            odl_yylval.keyword = keyword_delete;
                            return DELETE;
                          }
                        }

read                    { if (functionArgs) {
                            string_dupChar(&odl_yylval.text, yytext);
                            return WORD;
                          } else {
                            odl_yylval.keyword = keyword_read;
                            odl_yylval.attributes = acl_read;
                            return READ;
                          }
                        }

write                   { if (functionArgs) {
                            string_dupChar(&odl_yylval.text, yytext);
                            return WORD;
                          } else {
                            odl_yylval.keyword = keyword_write;
                            odl_yylval.attributes = acl_write;
                            return WRITE;
                          }
                        }

prewrite                { if (functionArgs) {
                            string_dupChar(&odl_yylval.text, yytext);
                            return WORD;
                          } else {
                            odl_yylval.keyword = keyword_prewrite;
                            return PREWRITE;
                          }
                        }

translate               { if (functionArgs) {
                            string_dupChar(&odl_yylval.text, yytext);
                            return WORD;
                          } else {
                            odl_yylval.keyword = keyword_translate;
                            return TRANSLATE;
                          }
                        }

execute                 { if (functionArgs) {
                            string_dupChar(&odl_yylval.text, yytext);
                            return WORD;
                          } else {
                            odl_yylval.attributes = acl_execute;
                            return EXECUTE;
                          }
                        }

read_value              { if (functionArgs) {
                            string_dupChar(&odl_yylval.text, yytext);
                            return WORD;
                          } else {
                            odl_yylval.attributes = acl_read_value;
                            return EXECUTE;
                          }
                        }

none                    { if (functionArgs) {
                            string_dupChar(&odl_yylval.text, yytext);
                            return WORD;
                          } else {
                            odl_yylval.attributes = 0;
                            return NONE;
                          }
                        }

with                    { if (functionArgs) {
                            string_dupChar(&odl_yylval.text, yytext);
                            return WORD;
                          } else {
                            odl_yylval.keyword = keyword_with;
                            return WITH;
                          }
                        }

using                   { if (functionArgs) {
                            string_dupChar(&odl_yylval.text, yytext);
                            return WORD;
                          } else {
                            odl_yylval.keyword = keyword_using;
                            return USING;
                          }
                        }

default                 { if (functionArgs) {
                            string_dupChar(&odl_yylval.text, yytext);
                            return WORD;
                          } else {
                            odl_yylval.keyword = keyword_default;
                            return DEFAULT;
                          }
                        }

constraint              { if (functionArgs) {
                            string_dupChar(&odl_yylval.text, yytext);
                            return WORD;
                          } else {
                            odl_yylval.keyword = keyword_constraint;
                            return CONSTRAINT;
                          }
                        }

read-only               { if (functionArgs) {
                            string_dupChar(&odl_yylval.text, yytext);
                            return WORD;
                          } else {
                            odl_yylval.attributes = attr_readonly;
                            return ATTRIBUTE;
                          }
                        }

persistent              { if (functionArgs) {
                            string_dupChar(&odl_yylval.text, yytext);
                            return WORD;
                          } else {
                            odl_yylval.attributes = attr_persistent;
                            return ATTRIBUTE;
                          }
                        }

transient|!persistent   { if (functionArgs) {
                            string_dupChar(&odl_yylval.text, yytext);
                            return WORD;
                          } else {
                            odl_yylval.attributes = attr_not_persistent;
                            return ATTRIBUTE;
                          }
                        }

upc                     { if (functionArgs) {
                            string_dupChar(&odl_yylval.text, yytext);
                            return WORD;
                          } else {
                            odl_yylval.attributes = attr_upc;
                            return ATTRIBUTE;
                          }
                        }

!upc                    { if (functionArgs) {
                            string_dupChar(&odl_yylval.text, yytext);
                            return WORD;
                          } else {
                            odl_yylval.attributes = attr_not_upc;
                            return ATTRIBUTE;
                          }
                        }

usersetting             { if (functionArgs) {
                            string_dupChar(&odl_yylval.text, yytext);
                            return WORD;
                          } else {
                            odl_yylval.attributes = attr_usersetting;
                            return ATTRIBUTE;
                          }
                        }

!usersetting            { if (functionArgs) {
                            string_dupChar(&odl_yylval.text, yytext);
                            return WORD;
                          } else {
                            odl_yylval.attributes = attr_not_usersetting;
                            return ATTRIBUTE;
                          }
                        }

overwrite_upc           { if (functionArgs) {
                            string_dupChar(&odl_yylval.text, yytext);
                            return WORD;
                          } else {
                            odl_yylval.attributes = attr_upc_overwrite;
                            return ATTRIBUTE;
                          }
                        }

key                     { if (functionArgs) {
                            string_dupChar(&odl_yylval.text, yytext);
                            return WORD;
                          } else {
                            odl_yylval.attributes = attr_key;
                            return ATTRIBUTE;
                          }
                        }

volatile                { if (functionArgs) {
                            string_dupChar(&odl_yylval.text, yytext);
                            return WORD;
                          } else {
                            odl_yylval.attributes = attr_volatile;
                            return ATTRIBUTE;
                          }
                        }

template-only           { if (functionArgs) {
                            string_dupChar(&odl_yylval.text, yytext);
                            return WORD;
                          } else {
                            odl_yylval.attributes = attr_templateonly;
                            return ATTRIBUTE;
                          }
                        }

async                   { if (functionArgs) {
                            string_dupChar(&odl_yylval.text, yytext);
                            return WORD;
                          } else {
                            odl_yylval.attributes = attr_async;
                            return ATTRIBUTE;
                          }
                        }

accept-parameters       { if (functionArgs) {
                            string_dupChar(&odl_yylval.text, yytext);
                            return WORD;
                          } else {
                            odl_yylval.attributes = attr_acceptparameters;
                            return ATTRIBUTE;
                          }
                        }

message                 { if (functionArgs) {
                            string_dupChar(&odl_yylval.text, yytext);
                            return WORD;
                          } else {
                            odl_yylval.attributes = attr_message;
                            return ATTRIBUTE;
                          }
                        }

in                      { odl_yylval.attributes = attr_in;
                          return ATTRIBUTE;
                        }

out                     { odl_yylval.attributes = attr_out;
                          return ATTRIBUTE;
                        }

mandatory               { odl_yylval.attributes = attr_mandatory;
                          return ATTRIBUTE;
                        }

void                    { odl_yylval.dataType=type_void;
                          return  DATATYPE;
                        }

string                  { odl_yylval.dataType=type_string;
                          return  DATATYPE;
                        }

int8                    { odl_yylval.dataType=type_int8;
                          return  DATATYPE;
                        }

int16                   { odl_yylval.dataType=type_int16;
                          return  DATATYPE;
                        }

int32                   { odl_yylval.dataType=type_int32;
                          return  DATATYPE;
                        }

int64                   { odl_yylval.dataType=type_int64;
                          return  DATATYPE;
                        }

uint8                   { odl_yylval.dataType=type_uint8;
                          return  DATATYPE;
                        }

uint16                  { odl_yylval.dataType=type_uint16;
                          return  DATATYPE;
                        }

uint32                  { odl_yylval.dataType=type_uint32;
                          return  DATATYPE;
                        }

uint64                  { odl_yylval.dataType=type_uint64;
                          return  DATATYPE;
                        }

bool                    { odl_yylval.dataType=type_bool;
                          return  DATATYPE;
                        }

datetime                { odl_yylval.dataType=type_date_time;
                          return  DATATYPE;
                        }

reference               { odl_yylval.dataType=type_reference;
                          return  DATATYPE;
                        }

list                    {  odl_yylval.dataType=type_list;
                           return  DATATYPE;
                        }

fd                      {  odl_yylval.dataType=type_file_descriptor;
                           return  DATATYPE;
                        }

byte_array              { odl_yylval.dataType=type_byte_array;
                          return  DATATYPE;
                        }

variant                 { odl_yylval.dataType=type_variant;
                          return  DATATYPE;
                        }

double                  { odl_yylval.dataType=type_double;
                          return DATATYPE;
                        }

minvalue                { if (functionArgs) {
                            string_dupChar(&odl_yylval.text, yytext);
                            return WORD;
                          } else {
                            odl_yylval.keyword = keyword_minvalue;
                            return MINVALUE;
                          }
                        }

maxvalue                { if (functionArgs) {
                            string_dupChar(&odl_yylval.text, yytext);
                            return WORD;
                          } else {
                            odl_yylval.keyword = keyword_maxvalue;
                            return MAXVALUE;
                          }
                        }

range                   { if (functionArgs) {
                            string_dupChar(&odl_yylval.text, yytext);
                            return WORD;
                          } else {
                            odl_yylval.keyword = keyword_range;
                            return RANGE;
                          }
                        }

enum                    { if (functionArgs) {
                            string_dupChar(&odl_yylval.text, yytext);
                            return WORD;
                          } else {
                            odl_yylval.keyword = keyword_enum;
                            return ENUM;
                          }
                        }

custom                  { if (functionArgs) {
                            string_dupChar(&odl_yylval.text, yytext);
                            return WORD;
                          } else {
                            odl_yylval.keyword = keyword_custom;
                            return CUSTOM;
                          }
                        }

datamodel               { if (functionArgs) {
                            string_dupChar(&odl_yylval.text, yytext);
                            return WORD;
                          } else {
                            odl_yylval.keyword = keyword_datamodel;
                            return DATAMODEL;
                          }
                        }

instance                { if (functionArgs) {
                            string_dupChar(&odl_yylval.text, yytext);
                            return WORD;
                          } else {
                            odl_yylval.keyword = keyword_instance;
                            return INSTANCE;
                          }
                        }

of                      { if (functionArgs) {
                            string_dupChar(&odl_yylval.text, yytext);
                            return WORD;
                          } else {
                            odl_yylval.keyword = keyword_of;
                            return OF;
                          }
                        }

destination             { if (functionArgs) {
                            string_dupChar(&odl_yylval.text, yytext);
                            return WORD;
                          } else {
                            odl_yylval.keyword = keyword_destination;
                            return DESTINATION;
                          }
                        }

from                    { if (functionArgs) {
                            string_dupChar(&odl_yylval.text, yytext);
                            return WORD;
                          } else {
                            odl_yylval.keyword = keyword_from;
                            return FROM;
                          }
                        }

parameter               { if (functionArgs) {
                            string_dupChar(&odl_yylval.text, yytext);
                            return WORD;
                          } else {
                            odl_yylval.keyword = keyword_parameter;
                            return PARAMETER;
                          }
                        }

function                { if (functionArgs) {
                            string_dupChar(&odl_yylval.text, yytext);
                            return WORD;
                          } else {
                            odl_yylval.keyword = keyword_function;
                            return FUNCTION;
                          }
                        }

recursive               { if (functionArgs) {
                            string_dupChar(&odl_yylval.text, yytext);
                            return WORD;
                          } else {
                            odl_yylval.keyword = keyword_recursive;
                            return RECURSIVE;
                          }
                        }

accept                  { if (functionArgs) {
                            string_dupChar(&odl_yylval.text, yytext);
                            return WORD;
                          } else {
                            odl_yylval.keyword = keyword_accept;
                            return ACCEPT;
                          }
                        }

drop                    { if (functionArgs) {
                            string_dupChar(&odl_yylval.text, yytext);
                            return WORD;
                          } else {
                            odl_yylval.keyword = keyword_drop;
                            return DROP;
                          }
                        }

rename                  { if (functionArgs) {
                            string_dupChar(&odl_yylval.text, yytext);
                            return WORD;
                          } else {
                            odl_yylval.keyword = keyword_rename;
                            return RENAME;
                          }
                        }

request                 { if (functionArgs) {
                            string_dupChar(&odl_yylval.text, yytext);
                            return WORD;
                          } else {
                            odl_yylval.keyword = keyword_rename;
                            return REQUEST;
                          }
                        }

cast                    { if (functionArgs) {
                            string_dupChar(&odl_yylval.text, yytext);
                            return WORD;
                          } else {
                            odl_yylval.keyword = keyword_cast;
                            return CAST;
                          }
                        }

counted                 { if (functionArgs) {
                            string_dupChar(&odl_yylval.text, yytext);
                            return WORD;
                          } else {
                            odl_yylval.keyword = keyword_counted;
                            return COUNTED;
                          }
                        }

deleted                 { if (functionArgs) {
                            string_dupChar(&odl_yylval.text, yytext);
                            return WORD;
                          } else {
                            odl_yylval.keyword = keyword_deleted;
                            return DELETED;
                          }
                        }

"..."                   { string_dupChar(&odl_yylval.text, yytext);
                          return DOTS;
                        }

"true"                  { string_dupChar(&odl_yylval.text, yytext);
                          return TEXT;
                        }

"false"                 { string_dupChar(&odl_yylval.text, yytext);
                          return TEXT;
                        }

-[[:digit:]]+           { string_dupChar(&odl_yylval.text, yytext);
                          return NUMBER;
                        }

[[:digit:]]+            { string_dupChar(&odl_yylval.text, yytext);
                          return NUMBER;
                        }

[a-zA-Z0-9\-_]+         { string_dupChar(&odl_yylval.text, yytext);
                          return WORD;
                        }

[a-zA-Z0-9\-_/]+         { string_dupChar(&odl_yylval.text, yytext);
                          return PATH;
                        }

\n|.                    { return yytext[0];
                        }

%%

static void start_description() {
#ifdef PCB_HELP_SUPPORT
  if (strcmp(getenv("PCB_DESCRIPTION")?getenv("PCB_DESCRIPTION"):"enabled","enabled") == 0) {
      yy_push_state( DESCRIPTION );
      string_clear(&description);
      string_clear(&description_line);
  } else {
      yy_push_state( COMMENT );
  }
#else
  yy_push_state( COMMENT );
#endif
}

static void string_dupChar(string_t **dest, const char *src) {
    *dest = calloc(1 ,sizeof(string_t));
    string_initialize(*dest, strlen(src) + 1);
    string_fromChar(*dest, src);
}
