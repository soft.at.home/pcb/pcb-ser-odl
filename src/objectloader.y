/****************************************************************************
**
** Copyright (c) 2020 SoftAtHome
** 
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
** 
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
** 
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
** 
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
** 
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
** 
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
** 
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
** 
** DISCLAIMER
** 
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

%{
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include <pwd.h>
#include <grp.h>
#include <libgen.h>

#include <dlfcn.h>

#include "objectloader.h"

#include <components.h>

#include <debug/sahtrace.h>

#include <pcb/common.h>
#include <pcb/utils.h>
#include <pcb/core.h>

#include <components.h>

#ifndef CONFIG_PCB_DEFAULT_PLUGIN_PATH
#define CONFIG_PCB_DEFAULT_PLUGIN_PATH "/usr/lib/pcb"
#endif

#ifdef CONFIG_PCB_ACL_USERMNGT
#include <usermngt/usermngt.h>
#endif

#include "odl_save.h"

typedef void (* pcb_plugin_enter_handler) (pcb_t *pcb);
typedef bool (* pcb_plugin_initialize_handler) (pcb_t *pcb, int argc, char *argv[]);
typedef void (* pcb_plugin_cleanup_handler) ();
typedef void (* pcb_plugin_exit_handler) ();

#define FUNCTION_WRAPPER(name) __##name

// flex/bison internal variables
#define YYERROR_VERBOSE
extern int   odl_yylex();
extern FILE  *odl_yyin;
extern int   odl_yylineno;

// loaded shared objects
typedef struct _library {
    void *handle;
    llist_iterator_t it;
    bool initialized;
    char *filename;
} library_t;

typedef struct _odl_file {
    llist_iterator_t it;
    char *file_name;
    int linenr;
    void *libraryHandle;
} odl_file_t;

#ifdef PCB_HELP_SUPPORT
string_t description;
string_t description_line;
#endif

static variant_list_t *include_files = NULL;
static variant_map_t *custom_types = NULL;
static variant_map_t *current_type;
static variant_map_t *file_locations = NULL;
static bool *definitions_available = NULL;

static bool verify = false;

static llist_t libraries;
static void *defaultLibraryHandle = NULL;

// datamodel variables
static datamodel_t             *dm               = NULL;
static object_t                *currentObject    = NULL;
static parameter_t             *currentParameter = NULL;
static function_t              *currentFunction  = NULL;
static object_destination_t    *currentDest      = NULL;
static mapping_rule_t          *currentRule      = NULL;
static char                    *currentMib       = NULL;
static variant_list_t          *enumList         = NULL;
static odl_file_t              *currentFile      = NULL;
static object_t                *restore          = NULL;

static int            fatalError = 0;
static int            error_depth = 0;
static pcb_t         *pcb;
static char          *input_dir = NULL;
llist_t               odl_file_stack;

bool functionArgs = false;

FILE *out;

static const char *pcb_name = NULL;

typedef struct yy_buffer_state *YY_BUFFER_STATE;

// function prototypes
static void                  openIncludeFile(const char *file);
static void                  getObject(const char *name);
static void                  createObject(const char *name, uint32_t attribute, int multi);
static void                  verifyMib(const char *name);
static void                  applyMib(const char *name);
static void                  applyParameterAttributes(parameter_t *parameter, uint32_t attributes);
static void                  setParameterAttributes(const char *name, uint32_t attributes);
static void                  applyInstanceAttributes(object_t *template, object_t *instance, uint32_t attributes);
static void                  extendInstance(const char *name, uint32_t index, const char *key, uint32_t attributes);
static void                  createInstance(const char *name, uint32_t index, const char *key, uint32_t attributes);
static void                  createMappedInstance(const char *name, uint32_t index, const char *key, const char *destination, bool recursive);
static void                  deleteInstance(const char *name, const char *key, uint32_t attribute);
static void                  createMappedObject(const char *name, const char *destination, bool recursive);
static void                  addMappingRule(rule_type_t type, rule_action_t action, const char *regexp);
static void                  objectDone();
static void                  setParameterValue(const char *parametername, const char *value);
static uint32_t              decode_object_attributes(int attributes);
static void                  createParameter(const char *name, const uint32_t attribute,const parameter_type_t type);
static void                  selectParameter(const char *name);
static void                  addDestination(const char *uri);
static void                  addDestinationParameter(const char *client, const char *plugin);
static void                  addDestinationFunction(const char *functionName, const char *destName);
static void                  setTranslator(const char *functionName, const char *sofile);
static uint32_t              decode_parameter_attributes(int attributes);
static parameter_type_t      decode_parameter_type(int type);
static void                  createFunction(const char *name, uint32_t attribute,const function_type_t type);
static void                  createCustomFunction(const char *name, uint32_t attribute,const char *type);
static void                  selectFunction(const char *name);
static uint32_t              decode_function_attributes(int attributes);
static function_type_t       decode_function_type(int type);
static void                  addArgument(const char *name, uint32_t attribute,const argument_type_t type);
static void                  addArgumentCustom(const char *name, uint32_t attribute,const char *type);
static uint32_t  decode_argument_attributes(int attributes);
static argument_type_t       decode_argument_type(int type);
static void                  addRuleRenameModifier(const char *functionName, const char *sofile);
static void                  addRuleTranslateModifier(const char *functionName, const char *sofile);
static void                  addRuleCastModifier(const char *functionName, const char *sofile);

static odl_file_t *push_odl_file(const char *file);
static void pop_odl_file();

static void setDefaultValue(const char *value);
static void setMinimumConstraint(const char *minValue);
static void setMaximumConstraint(const char *maxValue);
static void setRangeConstraint(const char *minValue,const char *maxValue);
static void setEnumConstraint();
static void addEnumValue(const char *value);
static void initEnumValue(const char *value);
static void setCustomConstraint(const char *functionName, const char *sofile);
static void setCallbackFunction(int keyword,const char *functionName, const char *sofile);

static void validateParameter(parameter_t *paramter);

static void addCustomType(const char *name);
static void addCustomTypeMember(const char *name, uint32_t type, const char *custom_type);

static void setDefaultLibrary(const char *filename, const char *flags);

static void *resolveFunction(const char *filename,const char *functionName);

static void setAcl(const char *value, uint16_t flags);

static void setInstanceCounter(const char *parameterName);

int odl_yylex_destroy (void );
void odl_yypop_buffer_state (void );
YY_BUFFER_STATE odl_yy_create_buffer (FILE *file,int size  );
void odl_yypush_buffer_state (YY_BUFFER_STATE new_buffer  );
int odl_yyerror(const char *msg);

%}

%union {
  string_t *text;
  int number;
  int keyword;
  int attributes;
  int dataType;
  int protocol;
}

%token <text>       WORD
%token <attributes> ATTRIBUTE
%token <dataType>   DATATYPE
%token <text>       NUMBER
%token <text>       TEXT
%token <text>       PATH
%token <text>       DOTS

%token <keyword>    OBJECT
%token <keyword>    CONSTRAINT
%token <keyword>    WITH
%token <keyword>    USING
%token <keyword>    DEFAULT
%token <keyword>    MINVALUE
%token <keyword>    MAXVALUE
%token <keyword>    RANGE
%token <keyword>    ENUM
%token <keyword>    CUSTOM
%token <keyword>    INCLUDE
%token <keyword>    FILE_DONE
%token <keyword>    DEFINE
%token <keyword>    ACL
%token <keyword>    USER
%token <keyword>    GROUP
%token <keyword>    WORLD
%token <keyword>    READ
%token <keyword>    WRITE
%token <keyword>    PREWRITE
%token <keyword>    ADD
%token <keyword>    DELETE
%token <keyword>    EXECUTE
%token <keyword>    NONE
%token <keyword>    DATAMODEL
%token <keyword>    INSTANCE
%token <keyword>    OF
%token <keyword>    DESTINATION
%token <keyword>    FROM
%token <keyword>    PARAMETER
%token <keyword>    TRANSLATE
%token <keyword>    FUNCTION
%token <keyword>    RECURSIVE
%token <keyword>    ACCEPT
%token <keyword>    DROP
%token <keyword>    RENAME
%token <keyword>    REQUEST
%token <keyword>    CAST
%token <keyword>    EXTENDS
%token <keyword>    MIB
%token <keyword>    EXTEND
%token <keyword>    COUNTED
%token <keyword>    DELETED

%type <text>    value
%type <number>  multi
%type <attributes> attributes
%type <attributes> aclAttributes
%type <attributes> aclAttribute

%type <keyword> objects
%type <keyword> object
%type <keyword> objectHeader
%type <keyword> objectBody
%type <keyword> objectData
%type <keyword> parameter
%type <keyword> parameterHeader
%type <keyword> parameterBody
%type <keyword> parameterData
%type <keyword> function
%type <keyword> acl
%type <keyword> handler
%type <keyword> constraint
%type <keyword> default
%type <keyword> customConstraint
%type <keyword> callback
%type <keyword> extend
%type <keyword> counted
%type <text> objectName
%type <text> enumeration
%type <text> counter

%start program
%%

program          : %empty
                 | data

data             : eof
                 | eof data
                 | header
                 | header eof
                 | header eof data
                 | header definition
                 | header definition eof
                 | header definition eof data
                 | header body
                 | header body eof
                 | header body eof data
                 | header definition body
                 | header definition body eof
                 | header definition body eof data
                 | definition
                 | definition eof
                 | definition eof data
                 | definition body
                 | definition body eof
                 | definition body eof data
                 | body
                 | body eof
                 | body eof data
                 ;

eof              : FILE_DONE                                      { SAH_TRACEZ_INFO("odl","File done %s",currentFile->file_name); pop_odl_file(); yy_state_set ( 0 ); }
                 ;

header           : header include
                 | header using
                 | include
                 | using
                 ;

include          : INCLUDE TEXT ';'                               { openIncludeFile(string_buffer($2)); string_cleanup($2); free($2); }
                 ;

using            : USING TEXT ';'                                 { setDefaultLibrary(string_buffer($2), NULL); string_cleanup($2); free($2); }
                 | USING TEXT WORD ';'                            { setDefaultLibrary(string_buffer($2), string_buffer($3)); string_cleanup($2); free($2); string_cleanup($3); free($3); }
                 ;

definition       : defines mibs acl
                 | defines acl mibs
                 | defines acl
                 | defines mibs
                 | defines
                 | mibs acl
                 | acl mibs
                 | acl
                 | mibs
                 ;

body             : '{' objectBody '}' objects datamodels
                 | '{' objectBody '}' objects
                 | '{' objectBody '}' datamodels
                 | '{' objectBody '}'
                 | objects datamodels
                 | objects
                 | datamodels
                 ;

defines          : defines define | define
                 ;

define           : defineHeader '{' defineBody '}' { current_type = NULL; }
                 ;

defineHeader     : DEFINE WORD { addCustomType(string_buffer($2)); string_cleanup($2); free($2); }
                 ;

defineBody       : defineBody defineMember | defineMember
                 ;

defineMember     : DATATYPE WORD ';' { addCustomTypeMember(string_buffer($2), $1, NULL); string_cleanup($2); free($2); }
                 | WORD WORD ';'     { addCustomTypeMember(string_buffer($2), 0, string_buffer($1)); string_cleanup($2); free($2); string_cleanup($1); free($1); }
                 ;

mibs             : mibs mib | mib
                 ;

mib              : mibHeader '{' objectBody '}'                  { objectDone(); currentObject = restore; }
                 | mibHeader '{' '}'                             { objectDone(); currentObject = restore; }
                 | mibHeader ';'                                 { objectDone(); currentObject = restore; }
                 ;

mibHeader        : MIB objectName                                { restore = currentObject;
                                                                   if (!verify) {
                                                                       currentObject = datamodel_mibRoot(dm);
                                                                   } else {
                                                                       *definitions_available = true;
                                                                   }
                                                                   createObject(string_buffer($2), object_attr_default, 0);
                                                                   string_cleanup($2);
                                                                   free($2);
                                                                 }
                 ;

objects          : objects object | object
                 ;

object           : objectHeader '{' objectBody '}'                { objectDone(); }
                 | objectHeader '{' '}'                           { objectDone(); }
                 | objectHeader ';'                               { objectDone(); }
                 ;

objectHeader     : OBJECT objectName multi                           { createObject(string_buffer($2), object_attr_template, $3); string_cleanup($2); free($2);                    }
                 | attributes OBJECT objectName multi                { createObject(string_buffer($3), decode_object_attributes($1)|object_attr_template, $4); string_cleanup($3); free($3); }
                 | OBJECT objectName                                 { createObject(string_buffer($2), object_attr_default, 0); string_cleanup($2); free($2);                      }
                 | attributes OBJECT objectName                      { createObject(string_buffer($3), decode_object_attributes($1), 0); string_cleanup($3); free($3);             }
                 | OBJECT EXTENDS objectName '(' NUMBER ',' TEXT ')' { extendInstance(string_buffer($3), (uint32_t)atoi(string_buffer($5)), string_buffer($7), 0);
                                                                       string_cleanup($3);
                                                                       string_cleanup($5);
                                                                       string_cleanup($7);
                                                                       free($3);
                                                                       free($5);
                                                                       free($7);
                                                                     }
                 | attributes OBJECT EXTENDS objectName '(' NUMBER ',' TEXT ')' { extendInstance(string_buffer($4), (uint32_t)atoi(string_buffer($6)), string_buffer($8), $1);
                                                                                  string_cleanup($4);
                                                                                  string_cleanup($6);
                                                                                  string_cleanup($8);
                                                                                  free($4);
                                                                                  free($6);
                                                                                  free($8);
                                                                                }
                 ;

objectName       : WORD                                           { $$ = $1; }
                 | TEXT                                           { $$ = $1; }
                 ;

multi            : '[' NUMBER ']'                                 { $$ = atoi(string_buffer($2)); string_cleanup($2); free($2);}
                 | '[' ']'                                        { $$ = INT_MAX;  }
                 ;

objectBody       : objectBody objectData | objectData
                 ;

objectData       : extend | parameter | function | handler | customConstraint | object | acl | counted
                 ;

extend           : extend_header mibs eof ';'                     { applyMib(currentMib); free(currentMib); }
                 | extend_header using mibs eof ';'               { applyMib(currentMib); free(currentMib); }
                 | extend_header define mibs eof ';'              { applyMib(currentMib); free(currentMib); }
                 | extend_header using define mibs eof ';'        { applyMib(currentMib); free(currentMib); }
                 | extend_header ';'                              { applyMib(currentMib); free(currentMib); }
                 ;

extend_header    : EXTEND USING MIB objectName                    { currentMib = strdup(string_buffer($4)); verifyMib(currentMib); string_cleanup($4); free($4); }
                 ;

acl              : ACL '{' aclBody '}'
                 ;

aclBody          : aclBody aclData | aclData
                 ;

aclData          : USER TEXT aclAttributes ';'                    { functionArgs = false; setAcl(string_buffer($2),$3); string_cleanup($2); free($2); }
                 | GROUP TEXT aclAttributes ';'                   { functionArgs = false; setAcl(string_buffer($2),$3 | acl_group_id); string_cleanup($2); free($2); }
                 | USER WORD aclAttributes ';'                    { functionArgs = false; setAcl(string_buffer($2),$3); string_cleanup($2); free($2); }
                 | GROUP WORD aclAttributes ';'                   { functionArgs = false; setAcl(string_buffer($2),$3 | acl_group_id); string_cleanup($2); free($2); }
                 | WORLD aclAttributes ';'                        { functionArgs = false; setAcl(NULL,$2); }
                 ;

aclAttributes    : aclAttributes ',' aclAttribute                 { $$ = $1 | $3; }
                 | aclAttribute                                   { $$ = $1; }
                 ;

aclAttribute     : READ                                           { $$ = $1; }
                 | WRITE                                          { $$ = $1; }
                 | EXECUTE                                        { $$ = $1; }
                 | NONE                                           { $$ = $1; }
                 ;

parameter        : parameterHeader '{' parameterBody '}'          { validateParameter(currentParameter); currentParameter = NULL; }
                 | parameterHeader '=' value ';'                  { setDefaultValue(string_buffer($3)); validateParameter(currentParameter); currentParameter = NULL; string_cleanup($3); free($3); }
                 | parameterHeader ';'                            { validateParameter(currentParameter); currentParameter = NULL;                      }
                 ;

parameterHeader  : DATATYPE WORD                                  { createParameter(string_buffer($2),parameter_attr_default,decode_parameter_type($1)); string_cleanup($2); free($2); }
                 | DATATYPE TEXT                                  { createParameter(string_buffer($2),parameter_attr_default,decode_parameter_type($1)); string_cleanup($2); free($2); }
                 | attributes DATATYPE WORD                       { createParameter(string_buffer($3),decode_parameter_attributes($1),decode_parameter_type($2)); string_cleanup($3); free($3); }
                 | attributes DATATYPE TEXT                       { createParameter(string_buffer($3),decode_parameter_attributes($1),decode_parameter_type($2)); string_cleanup($3); free($3); }
                 ;

parameterBody    : parameterBody parameterData | parameterData
                 ;

parameterData    : handler | constraint | default | acl
                 ;

function         : functionheader '(' functionArguments arg_end ';'                  { currentFunction = NULL; }
                 | functionheader '(' arg_end ';'                                    { currentFunction = NULL; }
                 | functionheader '(' functionArguments arg_end '{' functionBody '}' { currentFunction = NULL; }
                 | functionheader '(' arg_end '{' functionBody '}'                   { currentFunction = NULL; }
                 ;

functionheader   : DATATYPE WORD                                  { createFunction(string_buffer($2), function_attr_default, decode_function_type($1)); functionArgs = true; string_cleanup($2); free($2); }
                 | attributes  DATATYPE WORD                      { createFunction(string_buffer($3), decode_function_attributes($1), decode_function_type($2)); functionArgs = true; string_cleanup($3); free($3); }
                 | WORD WORD                                      { createCustomFunction(string_buffer($2), function_attr_default, string_buffer($1)); functionArgs = true; string_cleanup($1); free($1); string_cleanup($2); free($2); }
                 | attributes  WORD WORD                          { createCustomFunction(string_buffer($3), decode_function_attributes($1), string_buffer($2)); functionArgs = true; string_cleanup($2); free($2); string_cleanup($3); free($3); }
                 | DATATYPE TEXT                                  { createFunction(string_buffer($2), function_attr_default, decode_function_type($1)); functionArgs = true; string_cleanup($2); free($2); }
                 | attributes  DATATYPE TEXT                      { createFunction(string_buffer($3), decode_function_attributes($1), decode_function_type($2)); functionArgs = true; string_cleanup($3); free($3); }
                 | WORD TEXT                                      { createCustomFunction(string_buffer($2), function_attr_default, string_buffer($1)); functionArgs = true; string_cleanup($1); free($1); string_cleanup($2); free($2); }
                 | attributes  WORD TEXT                          { createCustomFunction(string_buffer($3), decode_function_attributes($1), string_buffer($2)); functionArgs = true; string_cleanup($2); free($2); string_cleanup($3); free($3); }
                 ;

functionArguments: functionArguments ',' argument
                 | functionArguments ',' DOTS                     { function_setVariadic(currentFunction, true); string_cleanup($3); free($3);}
                 | argument
                 | DOTS                                           { function_setVariadic(currentFunction, true); string_cleanup($1); free($1);}
                 ;

argument         : DATATYPE WORD                                  { addArgument(string_buffer($2), argument_attr_default, decode_argument_type($1)); string_cleanup($2); free($2); }
                 | attributes  DATATYPE WORD                      { addArgument(string_buffer($3), decode_argument_attributes($1), decode_argument_type($2)); string_cleanup($3); free($3); }
                 | WORD WORD                                      { addArgumentCustom(string_buffer($2),argument_attr_default, string_buffer($1)); string_cleanup($1); free($1); string_cleanup($2); free($2); }
                 | attributes  WORD WORD                          { addArgumentCustom(string_buffer($3),decode_argument_attributes($1),string_buffer($2)); string_cleanup($2); free($2); string_cleanup($3); free($3); }
                 ;

arg_end          : ')'                                            { functionArgs = false; }
                 ;

functionBody     : acl;

attributes       : attributes ATTRIBUTE                           { $$ = $1 | $2; }
                 | ATTRIBUTE                                      { $$ = $1;      }
                 ;

handler          : callback WITH WORD ';'                         { setCallbackFunction($1,string_buffer($3),NULL); string_cleanup($3); free($3);}
                 | callback WITH WORD USING TEXT ';'              { setCallbackFunction($1,string_buffer($3),string_buffer($5)); string_cleanup($3); free($3); string_cleanup($5); free($5); }
                 ;

callback         : READ                                           { $$ = keyword_read; }
                 | WRITE                                          { $$ = keyword_write; }
                 | PREWRITE                                       { $$ = keyword_prewrite; }
                 | ADD                                            { $$ = keyword_add; }
                 | DELETE                                         { $$ = keyword_delete; }
                 | MIB ADD                                        { $$ = keyword_mib_add; }
                 | MIB DELETE                                     { $$ = keyword_mib_del; }
                 ;

constraint       : CONSTRAINT MINVALUE NUMBER ';'                 { setMinimumConstraint(string_buffer($3)); string_cleanup($3); free($3); }
                 | CONSTRAINT MAXVALUE NUMBER ';'                 { setMaximumConstraint(string_buffer($3)); string_cleanup($3); free($3); }
                 | CONSTRAINT RANGE '[' NUMBER ',' NUMBER ']' ';' { setRangeConstraint(string_buffer($4),string_buffer($6)); string_cleanup($4); free($4); string_cleanup($6); free($6); }
                 | CONSTRAINT ENUM  '[' enumeration ']' ';'       { setEnumConstraint(); }
                 | customConstraint
                 ;

enumeration      : enumeration ',' value                          { addEnumValue(string_buffer($3)); string_cleanup($3); free($3); }
                 | value                                          { initEnumValue(string_buffer($1)); string_cleanup($1); free($1); }
                 ;

customConstraint : CONSTRAINT CUSTOM WORD ';'                     { setCustomConstraint(string_buffer($3),NULL); string_cleanup($3); free($3); }
                 | CONSTRAINT CUSTOM WORD USING TEXT ';'          { setCustomConstraint(string_buffer($3),string_buffer($5)); string_cleanup($3); free($3); string_cleanup($5); free($5); }
                 ;

default          : DEFAULT value ';'                              { setDefaultValue(string_buffer($2)); string_cleanup($2); free($2); }
                 ;

value            : TEXT | WORD | NUMBER
                 ;

counted          : COUNTED WITH counter ';'                       { setInstanceCounter(string_buffer($3)); string_cleanup($3); free($3); }
                 ;

counter          : TEXT | WORD
                 ;

datamodels       : datamodels datamodel | datamodel;

datamodel        : dm_header '{' dm_body '}'                      { if (!verify) { object_commit(currentObject); } }
                 | dm_header '{' acl dm_body '}'                  { if (!verify) { object_commit(currentObject); } }
                 ;

dm_header        : DATAMODEL
                 ;

dm_body          : dm_body dm_object
                 | dm_object
                 ;

dm_object        : dm_object_header '{' dm_object_body '}'          { objectDone(); }
                 | dm_object_header ';'                             { objectDone(); }
                 | dm_object_header '{' '}'                         { objectDone(); }
                 | '{' dm_object_body '}'
                 | dm_map_header ';'                                { objectDone(); }
                 | dm_map_header '{' dm_map_body '}'                { objectDone(); }
                 | dm_map_header '{'  '}'                           { objectDone(); }
                 | dm_deleted_header ';'
                 ;

dm_object_header : OBJECT objectName                                            { getObject(string_buffer($2)); string_cleanup($2); free($2); }
                 | INSTANCE OF objectName '(' NUMBER ',' TEXT ')'               { createInstance(string_buffer($3), (uint32_t)atoll(string_buffer($5)), string_buffer($7), 0); string_cleanup($3); free($3); string_cleanup($5); free($5); string_cleanup($7); free($7); }
                 | attributes INSTANCE OF objectName '(' NUMBER ',' TEXT ')'    { createInstance(string_buffer($4), (uint32_t)atoll(string_buffer($6)), string_buffer($8), $1); string_cleanup($4); free($4); string_cleanup($6); free($6); string_cleanup($8); free($8); }
                 ;

dm_map_header    : OBJECT objectName FROM TEXT                                        { createMappedObject(string_buffer($2),string_buffer($4),false); string_cleanup($2); free($2); string_cleanup($4); free($4); }
                 | INSTANCE OF objectName '(' NUMBER ',' TEXT ')' FROM TEXT           { createMappedInstance(string_buffer($3), (uint32_t)atoi(string_buffer($5)), string_buffer($7), string_buffer($10), false); string_cleanup($3); free($3); string_cleanup($5); free($5); string_cleanup($7); free($7); string_cleanup($10); free($10); }
                 | OBJECT objectName FROM TEXT RECURSIVE                              { createMappedObject(string_buffer($2),string_buffer($4),true); string_cleanup($2); free($2); string_cleanup($4); free($4); }
                 | INSTANCE OF objectName '(' NUMBER ',' TEXT ')' FROM TEXT RECURSIVE { createMappedInstance(string_buffer($3), (uint32_t)atoi(string_buffer($5)), string_buffer($7), string_buffer($10), true); string_cleanup($3); free($3); string_cleanup($5); free($5); string_cleanup($7); free($7); string_cleanup($10); free($10); }
                 ;

dm_deleted_header : DELETED INSTANCE OF objectName '(' TEXT ')'                 { deleteInstance(string_buffer($4), string_buffer($6), 0); string_cleanup($4); free($4); string_cleanup($6); free($6); }
                  | DELETED ATTRIBUTE INSTANCE OF objectName '(' TEXT ')'       { deleteInstance(string_buffer($5), string_buffer($7), $2); string_cleanup($5); free($5); string_cleanup($7); free($7); }
                  ;

dm_map_body      : dm_object
                 | dm_mapping_rules
                 ;

dm_mapping_rules : dm_mapping_rules dm_mapping_rule | dm_mapping_rule;

dm_mapping_rule  : dm_mapping_param ';'                         { currentRule = rule_parent(currentRule); }
                 | dm_mapping_param '{' dm_mapping_mods '}'     { currentRule = rule_parent(currentRule); }
                 | dm_mapping_func ';'                          { currentRule = rule_parent(currentRule); }
                 | dm_mapping_object ';'                        { currentRule = rule_parent(currentRule); }
                 | dm_mapping_object '{' dm_mapping_rules '}'   { currentRule = rule_parent(currentRule); }
                 ;

dm_mapping_param : ACCEPT PARAMETER TEXT                 { addMappingRule(rule_type_parameter, rule_action_accept, string_buffer($3)); string_cleanup($3); free($3); }
                 | DROP PARAMETER TEXT                   { addMappingRule(rule_type_parameter, rule_action_drop, string_buffer($3)); string_cleanup($3); free($3); }
                 | ACCEPT REQUEST PARAMETER TEXT         { addMappingRule(rule_type_parameter_request, rule_action_accept, string_buffer($4)); string_cleanup($4); free($4); }
                 | ACCEPT OBJECT PARAMETER TEXT          { addMappingRule(rule_type_parameter_object, rule_action_accept, string_buffer($4)); string_cleanup($4); free($4); }
                 | DROP REQUEST PARAMETER TEXT           { addMappingRule(rule_type_parameter_request, rule_action_drop, string_buffer($4)); string_cleanup($4); free($4); }
                 | DROP OBJECT PARAMETER TEXT            { addMappingRule(rule_type_parameter_object, rule_action_drop, string_buffer($4)); string_cleanup($4); free($4); }
                 ;

dm_mapping_func  : ACCEPT FUNCTION TEXT                  { addMappingRule(rule_type_function, rule_action_accept, string_buffer($3)); string_cleanup($3); free($3); }
                 | DROP FUNCTION TEXT                    { addMappingRule(rule_type_function, rule_action_drop, string_buffer($3)); string_cleanup($3); free($3); }
                 ;

dm_mapping_object: ACCEPT OBJECT TEXT                    { addMappingRule(rule_type_object, rule_action_accept, string_buffer($3)); string_cleanup($3); free($3); }
                 | DROP OBJECT TEXT                      { addMappingRule(rule_type_object, rule_action_drop, string_buffer($3)); string_cleanup($3); free($3); }
                 ;

dm_mapping_mods  : dm_mapping_mods dm_mapping_mod | dm_mapping_mod;

dm_mapping_mod   : RENAME WITH WORD ';'                  { addRuleRenameModifier(string_buffer($3),NULL); string_cleanup($3); free($3); }
                 | RENAME WITH WORD USING TEXT ';'       { addRuleRenameModifier(string_buffer($3), string_buffer($5)); string_cleanup($3); free($3); string_cleanup($5); free($5); }
                 | TRANSLATE WITH WORD ';'               { addRuleTranslateModifier(string_buffer($3), NULL); string_cleanup($3); free($3); }
                 | TRANSLATE WITH WORD USING TEXT ';'    { addRuleTranslateModifier(string_buffer($3), string_buffer($5)); string_cleanup($3); free($3); string_cleanup($5); free($5); }
                 | CAST WITH WORD ';'                    { addRuleCastModifier(string_buffer($3),NULL); free($3); }
                 | CAST WITH WORD USING TEXT ';'         { addRuleCastModifier(string_buffer($3), string_buffer($5)); string_cleanup($3); free($3); string_cleanup($5); free($5); }
                 ;

dm_object_body   : dm_object_body dm_object_data
                 | dm_object_data
                 ;

dm_object_data   : dm_object | dm_destination | dm_value | dm_func | acl | extend
                 ;

dm_destination   : dm_dest_header '{' dm_dest_body '}'            { currentDest = NULL; }
                 ;

dm_dest_header   : DESTINATION TEXT                               { addDestination(string_buffer($2)); string_cleanup($2); free($2); }
                 ;

dm_dest_body     : dm_dest_body dm_parameter
                 | dm_dest_body dm_function
                 | dm_parameter
                 | dm_function
                 ;

dm_parameter     : dm_param_header ';'                              { currentParameter = NULL; }
                 | dm_param_header dm_param_trans ';'               { currentParameter = NULL; }
                 | dm_param_header ',' dm_params dm_param_trans ';' { currentParameter = NULL; }
                 ;

dm_param_header  : PARAMETER WORD                                 { addDestinationParameter(string_buffer($2),string_buffer($2)); string_cleanup($2); free($2); }
                 | PARAMETER TEXT                                 { addDestinationParameter(string_buffer($2),string_buffer($2)); string_cleanup($2); free($2); }
                 | PARAMETER WORD FROM WORD                       { addDestinationParameter(string_buffer($2),string_buffer($4)); string_cleanup($2); free($2); string_cleanup($4); free($4); }
                 | PARAMETER TEXT FROM WORD                       { addDestinationParameter(string_buffer($2),string_buffer($4)); string_cleanup($2); free($2); string_cleanup($4); free($4); }
                 ;

dm_params        : dm_params ',' WORD                             { parameter_addMap(currentParameter,string_buffer($3),NULL); string_cleanup($3); free($3); }
                 | WORD                                           { parameter_addMap(currentParameter,string_buffer($1),NULL); string_cleanup($1); free($1); }
                 ;

dm_param_trans   : TRANSLATE WITH WORD                            { setTranslator( string_buffer($3), NULL); string_cleanup($3); free($3); }
                 | TRANSLATE WITH WORD USING TEXT                 { setTranslator( string_buffer($3), string_buffer($5)); string_cleanup($3); free($3); string_cleanup($5); free($5); }
                 ;

dm_function      : FUNCTION WORD ';'                               { addDestinationFunction( string_buffer($2), NULL ); string_cleanup($2); free($2); }
                 | FUNCTION WORD FROM WORD ';'                     { addDestinationFunction( string_buffer($2),string_buffer($4) ); string_cleanup($2); free($2); string_cleanup($4); free($4); }
                 ;

dm_value         : PARAMETER WORD '=' value ';'                   { setParameterValue(string_buffer($2),string_buffer($4)); string_cleanup($2); free($2); string_cleanup($4); free($4); }
                 | PARAMETER TEXT '=' value ';'                   { setParameterValue(string_buffer($2),string_buffer($4)); string_cleanup($2); free($2); string_cleanup($4); free($4); }
                 | attributes PARAMETER WORD '=' value ';'        { setParameterValue(string_buffer($3),string_buffer($5)); setParameterAttributes(string_buffer($3), $1); string_cleanup($3); free($3); string_cleanup($5); free($5); }
                 | attributes PARAMETER TEXT '=' value ';'        { setParameterValue(string_buffer($3),string_buffer($5)); setParameterAttributes(string_buffer($3), $1); string_cleanup($3); free($3); string_cleanup($5); free($5); }
                 | dm_value_header '{' dm_value_body '}'          { currentParameter = NULL; }
                 ;

dm_value_header  : PARAMETER WORD                                 { selectParameter(string_buffer($2));  string_cleanup($2); free($2); }
                 | PARAMETER TEXT                                 { selectParameter(string_buffer($2)); string_cleanup($2); free($2); }
                 | attributes PARAMETER WORD                      { setParameterAttributes(string_buffer($3), $1); string_cleanup($3); free($3); }
                 | attributes PARAMETER TEXT                      { setParameterAttributes(string_buffer($3), $1); string_cleanup($3); free($3); }
                 ;

dm_value_body    : dm_acl default
                 | dm_acl
                 | default
                 ;

dm_acl           : ACL '{' dm_aclBody '}'
                 ;

dm_aclBody       : dm_aclBody dm_aclData
                 | dm_aclData
                 ;

dm_aclData       : USER WORD aclAttributes ';'                    { functionArgs = false; if (currentParameter || currentFunction) { setAcl(string_buffer($2),$3); }; string_cleanup($2); free($2); }
                 | GROUP WORD aclAttributes ';'                   { functionArgs = false; if (currentParameter || currentFunction) { setAcl(string_buffer($2),$3 | acl_group_id); }; string_cleanup($2); free($2);  }
                 | USER TEXT aclAttributes ';'                    { functionArgs = false; if (currentParameter || currentFunction) { setAcl(string_buffer($2),$3); }; string_cleanup($2); free($2); }
                 | GROUP TEXT aclAttributes ';'                   { functionArgs = false; if (currentParameter || currentFunction) { setAcl(string_buffer($2),$3 | acl_group_id); }; string_cleanup($2); free($2); }
                 | WORLD aclAttributes ';'                        { functionArgs = false; if (currentParameter || currentFunction) { setAcl(NULL,$2); } }
                 ;

dm_func          : dm_func_header '{' dm_func_body '}'            { currentFunction = NULL; }
                 ;

dm_func_header   : FUNCTION WORD                                 { selectFunction(string_buffer($2)); string_cleanup($2); free($2); }
                 | FUNCTION TEXT                                 { selectFunction(string_buffer($2)); string_cleanup($2); free($2); }
                 ;

dm_func_body     : dm_acl
                 ;
%%

static bool openSearchPath(const string_t *search_path) {
    string_list_t files;
    bool rc = false;
    if (!string_list_initialize(&files)) {
        SAH_TRACEZ_ERROR("odl", "Failed to initialize search path string list");
        return false;
    }
    string_list_split(&files, search_path, ",", strlist_skip_empty_parts, string_case_sensitive);
    string_list_iterator_t *file_it = NULL;
    string_list_for_each(file_it, &files) {
        const string_t * file = string_list_iterator_data(file_it);
        if(!string_buffer(file) || !*string_buffer(file)) {
            continue;
        }
        SAH_TRACEZ_INFO("odl", "Try to load %s", string_buffer(file));
        odl_yyin = fopen(string_buffer(file), "r");
        if (!odl_yyin) {
            SAH_TRACEZ_NOTICE("odl", "Failed to open %s", string_buffer(file));
        } else {
            odl_yypush_buffer_state(odl_yy_create_buffer(odl_yyin, 16384)); // YY_BUF_SIZE
            yy_state_set(0);
            push_odl_file(string_buffer(file));
            rc = true;
            goto out;
        }
    }
out:
    string_list_cleanup(&files);
    return rc;
}

/*

=== todo ===
 - check for return values of object/parameter functions
 - fix callback functions
 - add more logical checks (e.g. 2 x defaults,...)

*/
static void openIncludeFile(const char *file) {
    if (verify) {
        *definitions_available = true;
        SAH_TRACEZ_INFO("odl","Adding include file: %s",file);
        variant_list_addChar(include_files, file);
        return;
    }

    FILE *current = odl_yyin;
    SAH_TRACEZ_INFO("odl","Open include file %s",file);
    const char *plugin_path_default = CONFIG_PCB_DEFAULT_PLUGIN_PATH;
    const char *plugin_path = getenv("PCB_PLUGIN_PATH");
    const char *prefix_path = getenv("PCB_PREFIX_PATH");
    if (!plugin_path) {
        plugin_path = plugin_path_default;
    }


    /* search include files in the following order
        0. starts with "/" => absolute path
        else
        1. use "/etc/defaults" + filename
        2. input_dir + filename
        3. filename
        4. use "/usr/lib/" + name + filename
        5. use PCB_PLUGIN_PATH + filename
        6. path relative to includer + filename
    */

    if (file && *file) {
        string_t search_path;
        if (!string_initialize(&search_path, 1024)) {
            goto fail;
        }

        if (file[0] == '/') {
            SAH_TRACEZ_INFO("odl","Try to load %s",file);
            if (prefix_path && *prefix_path) {
                string_appendFormat(&search_path, "%s%s", prefix_path, file);
            } else {
                string_appendChar(&search_path, file);
            }
        } else {
            // 1. use "/etc/defaults/" + filename
            string_appendFormat(&search_path, "/etc/defaults/%s,", file);

            // 2. input_dir + filename
            if (input_dir && *input_dir) {
                string_appendFormat(&search_path, "%s/%s,", input_dir, file);
            }

            // 3. filename
            string_appendFormat(&search_path, "%s,", file);

            // 4. use "/usr/lib/" + name + filename
            string_appendFormat(&search_path, "/usr/lib/%s/%s,", pcb_name, file);

            // 5. PCB_PLUGIN_PATH + filename
            string_appendFormat(&search_path, "%s/%s,", plugin_path, file);
            
            // 6. path relative to includer + filename
            llist_iterator_t *it = llist_first(&odl_file_stack);
            if (it != NULL){
                odl_file_t *parentFile = llist_item_data(it, odl_file_t, it);
                char *parentFileName = strdup(parentFile->file_name);
                const char *parentDir = dirname(parentFileName);
                string_appendFormat(&search_path, "%s/%s,", parentDir, file);
                free(parentFileName);
            }
        }
        openSearchPath(&search_path);
        string_cleanup(&search_path);
    } else {
        goto fail;
    }

    if (!odl_yyin) {
        goto fail;
    }
    return;
fail:
    fprintf(out,"%s:%d: error: Failed to open include file %s\n",currentFile->file_name,odl_yylineno,file);
    SAH_TRACEZ_ERROR("odl","%s @ %d - Failed to open include file %s",currentFile->file_name,odl_yylineno,file);
    odl_yyin = current;
    fatalError = 1;
    return;

}

static void getObject(const char *name) {
  if (verify) {
    return;
  }
  char *Name = string_resolveEnvVarChar(name?name:"");
  currentObject = object_getObject(currentObject,Name,0,NULL);
  if (!currentObject) {
    fprintf(out,"%s:%d: error: object %s not found\n",currentFile->file_name,odl_yylineno,Name);
    SAH_TRACEZ_ERROR("odl","%s @ %d - object %s not found",currentFile->file_name,odl_yylineno,Name);
    fatalError = 1;
  }
  free(Name);
}

static void createObject(const char *name, uint32_t attribute, int multi)
{
  if (verify) {
    *definitions_available = true;
    return;
  }
  if (error_depth) {
    error_depth++;
    return;
  }

  object_t *parentObject;

  parentObject = currentObject;

  char *Name = string_resolveEnvVarChar(name?name:"");
  SAH_TRACEZ_INFO("odl","Creating object %s",Name);
  if (!Name || !(*Name)) {
    fprintf(out,"%s:%d: error: object must have a name\n",currentFile->file_name,odl_yylineno);
    SAH_TRACEZ_ERROR("odl","%s @ %d - object must have a name",currentFile->file_name,odl_yylineno);
    free(Name);
#ifdef PCB_HELP_SUPPORT
    string_clear(&description);
#endif
    fatalError = 1;
    return;
  }

  currentObject = object_create(parentObject,Name,attribute);
  if (!currentObject) {
    currentObject = object_getObject(parentObject,Name,0,NULL);
    if (currentObject) {
      SAH_TRACEZ_NOTICE("odl","%s @ %d - object %s already existing - extending definition",currentFile->file_name,odl_yylineno,Name);
    }
  }
  if(!currentObject) {
    fprintf(out,"%s:%d: error: Cannot create object %s\n",currentFile->file_name,odl_yylineno,Name);
    SAH_TRACEZ_ERROR("odl","%s @ %d - Cannot create object %s",currentFile->file_name,odl_yylineno,Name);
    fatalError = 1;
    currentObject = parentObject;
    free(Name);
#ifdef PCB_HELP_SUPPORT
    string_clear(&description);
#endif
    return;
  }

#ifdef PCB_HELP_SUPPORT
    const char *desc = getenv("PCB_DESCRIPTION")?getenv("PCB_DESCRIPTION"):"enabled";
    if (strcmp(desc,"enabled") == 0) {
        if (!string_isEmpty(&description)) {
            string_replaceChar(&description,"\"","'");
            string_appendFormat(&description,"\\n@defined %s:%d",currentFile->file_name,odl_yylineno);
        } else {
          string_fromChar(&description,"@defined ");
          string_appendFormat(&description,"%s:%d",currentFile->file_name,odl_yylineno);
        }
        object_setDescription(currentObject,string_buffer(&description));
    }
    string_clear(&description);
#endif

  free(Name);

  if(multi && object_isTemplate(currentObject))
    object_setMaxInstances(currentObject,multi);
}

static void verifyMib(const char *name) {
    if (verify) {
        return;
    }

    if (object_isMib(currentObject)) {
        fprintf(out,"%s:%d: error: MIBs can not be extended with MIBs\n",currentFile->file_name,odl_yylineno);
        SAH_TRACEZ_ERROR("odl","%s @ %d - Mibs can not be extended with mibs",currentFile->file_name,odl_yylineno);
        fatalError = 1;
        return;
    }

    char *Name = string_resolveEnvVarChar(name?name:"");
    SAH_TRACEZ_INFO("odl","Apply mib %s",Name);
    if (!Name || !(*Name)) {
        fprintf(out,"%s:%d: error: MIB must have a name\n",currentFile->file_name,odl_yylineno);
        SAH_TRACEZ_ERROR("odl","%s @ %d - MIB must have a name",currentFile->file_name,odl_yylineno);
        free(Name);
        fatalError = 1;
        return;
    }

    if (!datamodel_mib(dm,Name)) {
        FILE *current = odl_yyin;
        SAH_TRACEZ_INFO("odl","Mib %s not available, try to load",Name);
        char *mibfile = datamodel_mibFile(dm,Name);
        const char *prefix_path = getenv("PCB_PREFIX_PATH");
        char filename[1024] = "";
        snprintf(filename,1023,"%s%s%s",prefix_path?prefix_path:"",prefix_path?"/":"",mibfile);
        odl_yyin = fopen(filename, "r");
        if (!odl_yyin) {
            SAH_TRACEZ_NOTICE("odl","Failed to open %s",filename);
            odl_yyin = current;
            fatalError = 1;
        } else {
            odl_yypush_buffer_state(odl_yy_create_buffer(odl_yyin, 16384)); // YY_BUF_SIZE
            yy_state_set ( 0 );
            push_odl_file(filename);
        }
        free(mibfile);
    }

    free(Name);
    return;
}

static void applyMib(const char *name) {
    if (verify) {
        return;
    }

    char *Name = string_resolveEnvVarChar(name?name:"");
    SAH_TRACEZ_INFO("odl","Apply mib %s",Name);
    if (!Name || !(*Name)) {
        fprintf(out,"%s:%d: error: MIB must have a name\n",currentFile->file_name,odl_yylineno);
        SAH_TRACEZ_ERROR("odl","%s @ %d - MIB must have a name",currentFile->file_name,odl_yylineno);
        free(Name);
        fatalError = 1;
        return;
    }

    if (!datamodel_mib(dm,Name)) {
        fprintf(out,"%s:%d: error: MIB %s not found\n",currentFile->file_name,odl_yylineno, Name);
        SAH_TRACEZ_ERROR("odl","%s @ %d - MIB %s not found",currentFile->file_name,odl_yylineno, Name);
        free(Name);
        fatalError = 1;
        return;
    }

    if (!object_addMib(currentObject, Name)) {
        fprintf(out,"%s:%d: error: Failed to add MIB %s to object\n",currentFile->file_name,odl_yylineno, Name);
        SAH_TRACEZ_ERROR("odl","%s @ %d - Failed to add MIB %s to object",currentFile->file_name,odl_yylineno, Name);
        free(Name);
        fatalError = 1;
        return;
    }

    free(Name);
    return;
}

typedef struct _applyParameterAttributes_t {
    uint32_t attr_set;
    uint32_t attr_clear;
    uint32_t attr_parameter;
    void (*f)(parameter_t *, const bool);
} applyParameterAttributes_t;

static void applyParameterAttributes(parameter_t *parameter, uint32_t attributes) {
    unsigned int i = 0;
    /*
     * Because some attributes indirectly set other attributes, the order of the struct is important.
     * UserSetting implies UPC, UPC implies Persistent. Only when setting, not clearing.
     */
    applyParameterAttributes_t aia[] = {
        {
            .attr_set       = attr_usersetting,
            .attr_clear     = attr_not_usersetting,
            .attr_parameter = parameter_attr_upc_usersetting,
            .f              = parameter_setUserSetting,
        },
        {
            .attr_set       = attr_upc,
            .attr_clear     = attr_not_upc,
            .attr_parameter = parameter_attr_upc,
            .f              = parameter_setUPC,
        },
        {
            .attr_set       = attr_persistent,
            .attr_clear     = attr_not_persistent,
            .attr_parameter = parameter_attr_persistent,
            .f              = parameter_setPersistent,
        },
        {
            .attr_set       = attr_upc_overwrite,
            .attr_clear     = 0x0,
            .attr_parameter = parameter_attr_upc_overwrite,
            .f              = parameter_setUPCOverwrite,
        },
    };

    for (i = 0; i < sizeof(aia) / sizeof(applyParameterAttributes_t); i++) {
        if (attributes & aia[i].attr_set) {
            aia[i].f(parameter, true);
        } else if (attributes & aia[i].attr_clear) {
            aia[i].f(parameter, false);
        } else {
            if (parameter_attributes(parameter) & aia[i].attr_parameter) {
                aia[i].f(parameter, true);
            } else {
                aia[i].f(parameter, false);
            }
        }
    }
}

static void setParameterAttributes(const char *name, uint32_t attributes) {
    selectParameter(name);
    applyParameterAttributes(currentParameter, attributes);
}

typedef struct _applyInstanceAttributes_t {
    uint32_t attr_set;
    uint32_t attr_clear;
    uint32_t attr_object;
    void (*f)(object_t *, const bool);
} applyInstanceAttributes_t;

static void applyInstanceAttributes(object_t *template, object_t *instance, uint32_t attributes) {
    unsigned int i = 0;
    /*
     * Because some attributes indirectly set other attributes, the order of the struct is important.
     * UserSetting implies UPC, UPC implies Persistent. Only when setting, not clearing.
     */
    applyInstanceAttributes_t aia[] = {
        {
            .attr_set    = attr_usersetting,
            .attr_clear  = attr_not_usersetting,
            .attr_object = object_attr_upc_usersetting,
            .f           = object_setUserSetting,
        },
        {
            .attr_set    = attr_upc,
            .attr_clear  = attr_not_upc,
            .attr_object = object_attr_upc,
            .f           = object_setUPC,
        },
        {
            .attr_set    = attr_persistent,
            .attr_clear  = attr_not_persistent,
            .attr_object = object_attr_persistent,
            .f           = object_setPersistent,
        },
        {
            .attr_set    = attr_upc_overwrite,
            .attr_clear  = 0x0,
            .attr_object = object_attr_upc_overwrite,
            .f           = object_setUPCOverwrite,
        },
    };

    for (i = 0; i < sizeof(aia) / sizeof(applyInstanceAttributes_t); i++) {
        if (attributes & aia[i].attr_set) {
            aia[i].f(instance, true);
        } else if (attributes & aia[i].attr_clear) {
            aia[i].f(instance, false);
        } else {
            if (object_attributes(template) & aia[i].attr_object) {
                aia[i].f(instance, true);
            } else {
                aia[i].f(instance, false);
            }
        }
    }
}

static void extendInstance(const char *name, uint32_t index, const char *key, uint32_t attributes) {
  if (verify) {
    return;
  }
  if (error_depth) {
    error_depth+=2;
    return;
  }
  char *Name = string_resolveEnvVarChar(name?name:"");

  currentObject = object_getObject(currentObject,Name,0,NULL);
  if (!currentObject) {
    fprintf(out,"%s:%d: error: object %s not found\n",currentFile->file_name,odl_yylineno,Name);
    SAH_TRACEZ_ERROR("odl","%s @ %d - object %s not found",currentFile->file_name,odl_yylineno,Name);
    fatalError = 1;
    free(Name);
    return;
  }

  char *resolved = NULL;
  object_t *instance = NULL;
  if (key && (*key)) {
    resolved = string_resolveEnvVarChar(key?key:"");
    instance = object_getObject(currentObject,resolved,path_attr_key_notation,NULL);
  }

  SAH_TRACEZ_INFO("odl","Checking instance, key = %s", resolved);

  if (!instance) {
    SAH_TRACEZ_INFO("odl","Create instance, key = %s", resolved);
    instance = object_createInstance(currentObject,index,resolved);
  }

  free(resolved);
  if (!instance) {
    fprintf(out,"%s:%d: error: failed to create instance for %s\n",currentFile->file_name,odl_yylineno,Name);
    SAH_TRACEZ_ERROR("odl","%s @ %d - failed to create instance for %s",currentFile->file_name,odl_yylineno,Name);
    fatalError = 1;
    free(Name);
#ifdef PCB_HELP_SUPPORT
    string_clear(&description);
#endif
  } else {
    applyInstanceAttributes(currentObject, instance, attributes);
    currentObject = instance;
  }

#ifdef PCB_HELP_SUPPORT
    const char *desc = getenv("PCB_DESCRIPTION")?getenv("PCB_DESCRIPTION"):"enabled";
    if (strcmp(desc,"enabled") == 0) {
        if (!string_isEmpty(&description)) {
            string_replaceChar(&description,"\"","'");
            string_appendFormat(&description,"\\n@defined %s:%d",currentFile->file_name,odl_yylineno);
        } else {
          string_fromChar(&description,"@defined ");
          string_appendFormat(&description,"%s:%d",currentFile->file_name,odl_yylineno);
        }
        object_setDescription(currentObject,string_buffer(&description));
    }
    string_clear(&description);
#endif

  free(Name);
}

static void createInstance(const char *name, uint32_t index, const char *key, uint32_t attributes) {
  if (verify) {
    return;
  }
  if (error_depth) {
    error_depth += 1;
    return;
  }
  char *Name = string_resolveEnvVarChar(name?name:"");

  object_t *temp = object_getObject(currentObject,Name,0,NULL);
  if (!temp) {
    fprintf(out,"%s:%d: error: object %s not found\n",currentFile->file_name,odl_yylineno,Name);
    SAH_TRACEZ_NOTICE("odl","%s @ %d - object %s not found",currentFile->file_name,odl_yylineno,Name);
    free(Name);
    error_depth = 1;
    return;
  }
  currentObject = temp;

  char *resolved = NULL;
  object_t *instance = NULL;
  if (key && (*key)) {
    resolved = string_resolveEnvVarChar(key?key:"");
    instance = object_getObject(currentObject,resolved,path_attr_key_notation,NULL);
  }

  SAH_TRACEZ_INFO("odl","Checking instance, key = %s", resolved);

  if (!instance) {
    instance = object_createInstance(currentObject,index,resolved);
  }
  free(resolved);
  if (!instance) {
    fprintf(out,"%s:%d: error: failed to create instance for %s\n",currentFile->file_name,odl_yylineno,Name);
    SAH_TRACEZ_ERROR("odl","%s @ %d - failed to create instance for %s",currentFile->file_name,odl_yylineno,Name);
    fatalError = 1;
  } else {
    applyInstanceAttributes(currentObject, instance, attributes);
    currentObject = instance;
  }
  free(Name);
}

static void createMappedInstance(const char *name, uint32_t index, const char *key, const char *destination, bool recursive) {
  if (verify) {
    return;
  }
  char *Name = string_resolveEnvVarChar(name?name:"");

  currentObject = object_getObject(currentObject,Name,0,NULL);
  if (!currentObject) {
    fprintf(out,"%s:%d: error: object %s not found\n",currentFile->file_name,odl_yylineno,Name);
    SAH_TRACEZ_ERROR("odl","%s @ %d - object %s not found",currentFile->file_name,odl_yylineno,Name);
    fatalError = 1;
    free(Name);
    return;
  }

  char *resolved = string_resolveEnvVarChar(key?key:"");

  SAH_TRACEZ_INFO("odl","search %s instance of %s",resolved, object_name(currentObject,path_attr_key_notation));

  object_t *instance = object_getObject(currentObject,resolved,path_attr_key_notation,NULL);
  if (!instance) {
    instance = object_createInstance(currentObject,index,resolved);
    SAH_TRACEZ_INFO("odl","created instance %s.%s", object_name(currentObject,path_attr_key_notation), object_name(instance,path_attr_key_notation));
  } else {
    SAH_TRACEZ_INFO("odl","found instance %s.%s", object_name(currentObject,path_attr_key_notation), object_name(instance,path_attr_key_notation));
  }
  if (!instance) {
    fprintf(out,"%s:%d: error: failed to create instance for %s\n",currentFile->file_name,odl_yylineno,Name);
    SAH_TRACEZ_ERROR("odl","%s @ %d - failed to create instance for %s",currentFile->file_name,odl_yylineno,Name);
    fatalError = 1;
  } else {
    string_t base;
    string_initialize(&base,64);
    string_fromChar(&base,destination);
    string_resolveEnvVar(&base);

    object_destination_t *dest = NULL;

    dest = object_map(instance,string_buffer(&base),recursive);
    if (!dest) {
      string_cleanup(&base);
      fprintf(out,"%s:%d: error: failed to map object %s.%s to %s\n",currentFile->file_name,odl_yylineno,Name,object_name(instance,path_attr_key_notation),destination);
      SAH_TRACEZ_ERROR("odl","ERROR - %s:%d - failed to map object %s.%s to %s",currentFile->file_name,odl_yylineno,Name,object_name(instance,path_attr_key_notation),destination);
      fatalError = 1;
      return;
    }
    currentObject = instance;
    string_cleanup(&base);
  }
  free(resolved);
  free(Name);
}

static void deleteInstance(const char *name, const char *key, uint32_t attribute) {
  char *Name = NULL, *resolved = NULL;
  object_t *temp = NULL, *instance = NULL;

  (void)attribute;

  if (verify) {
    return;
  }
  if (error_depth) {
    error_depth += 1;
    return;
  }

  Name = string_resolveEnvVarChar(name ? name : "");

  temp = object_getObject(currentObject, Name, 0, NULL);
  if (!temp) {
    fprintf(out, "%s:%d: error: object %s not found\n", currentFile->file_name, odl_yylineno, Name);
    SAH_TRACEZ_NOTICE("odl", "%s @ %d - object %s not found", currentFile->file_name, odl_yylineno, Name);
    free(Name);
    error_depth = 1;
    return;
  }

  if (key && (*key)) {
    resolved = string_resolveEnvVarChar(key ? key : "");
    instance = object_getObject(temp, resolved, path_attr_key_notation, NULL);
  }

  SAH_TRACEZ_INFO("odl", "Checking instance, key = %s", resolved);

  if (instance) {
    object_delete(instance);
  }

  free(Name);
  free(resolved);
}

static void createMappedObject(const char *name, const char *destination, bool recursive) {
  if (verify) {
    return;
  }
  char *Name = string_resolveEnvVarChar(name?name:"");
  currentObject = object_getObject(currentObject,Name,0,NULL);
  if (!currentObject) {
    fprintf(out,"%s:%d: error: object %s not found\n",currentFile->file_name,odl_yylineno,Name);
    SAH_TRACEZ_ERROR("odl","%s @ %d - object %s not found",currentFile->file_name,odl_yylineno,Name);
    fatalError = 1;
    free(Name);
    return;
  }

  string_t base;
  string_initialize(&base,64);
  string_fromChar(&base,destination);
  string_resolveEnvVar(&base);

  object_destination_t *dest = NULL;

  dest = object_map(currentObject,string_buffer(&base),recursive);
  if (!dest) {
    fprintf(out,"%s:%d: error: failed to map object %s to %s\n",currentFile->file_name,odl_yylineno,Name,destination);
    SAH_TRACEZ_ERROR("odl","ERROR - %s:%d - failed to map object %s to %s",currentFile->file_name,odl_yylineno,Name,destination);
    fatalError = 1;
  }

  free(Name);
  string_cleanup(&base);
}

static void addMappingRule(rule_type_t type, rule_action_t action, const char *regexp) {
  if (verify) {
    return;
  }
  object_destination_t *dest = object_firstDestination(currentObject);
  mapping_rule_t *rule = NULL;
  if (currentRule) {
    rule = rule_createRule(currentRule,type,action);
  } else {
    rule = destination_createRule(dest,type,action);
  }
  if (!rule) {
    fprintf(out,"%s:%d: error: failed to create mapping rule\n",currentFile->file_name,odl_yylineno);
    SAH_TRACEZ_ERROR("odl","ERROR - %s:%d - failed to create mapping rule",currentFile->file_name,odl_yylineno);
    fatalError = 1;
    return;
  }
  if (!rule_setRegExp(rule,regexp)) {
      fprintf(out,"%s:%d: error: failed to create mapping rule\n",currentFile->file_name,odl_yylineno);
      SAH_TRACEZ_ERROR("odl","ERROR - %s:%d - failed to create mapping rule",currentFile->file_name,odl_yylineno);
      fatalError = 1;
      rule_delete(rule);
      return;
  }
  currentRule = rule;
}

static void objectDone() {
  if (verify) {
    return;
  }
  if (error_depth) {
    SAH_TRACEZ_INFO("odl", "Error depth = %d", error_depth);
    error_depth--;
    return;
  }
  if (object_isInstance(currentObject)) {
    currentObject = object_parent(object_parent(currentObject));
  } else {
    currentObject = object_parent(currentObject);
  }
}

static void setParameterValue(const char *parametername, const char *value) {
  if (verify || error_depth) {
    return;
  }
  char *Name = string_resolveEnvVarChar(parametername?parametername:"");
  parameter_t *parameter = object_getParameter(currentObject, Name);
  if (!parameter) {
    if (object_attributes(currentObject) & object_attr_accept_parameters) {
        parameter = parameter_create(currentObject, Name, parameter_type_string, parameter_attr_default|parameter_attr_persistent);
    }
    if (!parameter) {
      SAH_TRACEZ_WARNING("odl","%s @ %d - parameter %s not found, skipping set value",currentFile->file_name,odl_yylineno,Name);
      free(Name);
      return;
    }
  }

  string_t data;
  string_initialize(&data,0);
  char *resolved = string_resolveEnvVarChar(value?value:"");
  string_fromChar(&data,resolved);
  free(resolved);
  string_replaceChar(&data,"\\\"","\"");
  string_replaceChar(&data,"\\$","$");
  string_replaceChar(&data,"\\(","(");
  string_replaceChar(&data,"\\)",")");
  string_replaceChar(&data,"\\\\","\\");

  if (!parameter_setFromChar(parameter,string_buffer(&data))) {
      fprintf(out,"%s:%d: error: Cannot set value for parameter %s (value = %s)\n",currentFile->file_name,odl_yylineno,Name,string_buffer(&data));
      SAH_TRACEZ_ERROR("odl","%s @ %d - Cannot set value for parameter %s (value = %s)",currentFile->file_name,odl_yylineno,Name,string_buffer(&data));
      fatalError = 1;
  }
  string_cleanup(&data);
  free(Name);
}

static uint32_t decode_object_attributes(int attributes) {
    uint32_t decoded_attributes = object_attr_default;

    if(attributes & attr_readonly)         decoded_attributes |= object_attr_read_only;
    if(attributes & attr_template)         decoded_attributes |= object_attr_template;
    if(attributes & attr_acceptparameters) decoded_attributes |= object_attr_accept_parameters;
    if(attributes & attr_persistent)       decoded_attributes |= object_attr_persistent;
    if(attributes & attr_upc)              decoded_attributes |= object_attr_persistent|object_attr_upc;
    if(attributes & attr_usersetting)      decoded_attributes |= object_attr_persistent|object_attr_upc|object_attr_upc_usersetting;
    if(attributes & attr_upc_overwrite)    decoded_attributes |= object_attr_upc_overwrite;

    return decoded_attributes;
}

static void createParameter(const char *name, const uint32_t attribute,const parameter_type_t type) {
  if (verify || error_depth) {
      *definitions_available = true;
      return;
  }
  char *Name = string_resolveEnvVarChar(name?name:"");
  currentParameter = parameter_create(currentObject,Name,type,attribute);
  if(!currentParameter) {
    currentParameter = object_getParameter(currentObject,Name);
  }
  if(!currentParameter) {
    fprintf(out,"%s:%d: error: Cannot create parameter %s\n",currentFile->file_name,odl_yylineno, Name);
    SAH_TRACEZ_ERROR("odl","%s @ %d - Cannot create parameter %s",currentFile->file_name,odl_yylineno,Name);
    fatalError = 1;
#ifdef PCB_HELP_SUPPORT
    string_clear(&description);
#endif
  } else {
#ifdef PCB_HELP_SUPPORT
    const char *desc = getenv("PCB_DESCRIPTION")?getenv("PCB_DESCRIPTION"):"enabled";
    if (strcmp(desc,"enabled") == 0) {
        if (!string_isEmpty(&description)) {
            string_replaceChar(&description,"\"","'");
            string_appendFormat(&description,"\\n@defined %s:%d",currentFile->file_name,odl_yylineno);
        } else {
          string_fromChar(&description,"@defined ");
          string_appendFormat(&description,"%s:%d",currentFile->file_name,odl_yylineno);
        }
        parameter_setDescription(currentParameter,string_buffer(&description));
    }
    string_clear(&description);
#endif
  }
  free(Name);
}

static void selectParameter(const char *name) {
  char *Name = string_resolveEnvVarChar(name?name:"");
  currentParameter = object_getParameter(currentObject,Name);
  if(!currentParameter) {
    // create parameter if does not exist
    if (object_attributes(currentObject) & object_attr_accept_parameters) {
      currentParameter = parameter_create(currentObject, Name, parameter_type_string, parameter_attr_default|parameter_attr_persistent);
    }
    if (!currentParameter) {
      fprintf(out,"%s:%d: warning: Parameter %s not found\n",currentFile->file_name,odl_yylineno, Name);
      SAH_TRACEZ_WARNING("odl","%s @ %d - Parameter %s not found",currentFile->file_name,odl_yylineno,Name);
    }
  }
  free(Name);
}

static void addDestination(const char *uri) {
  if (verify) {
    return;
  }
  string_t base;
  string_initialize(&base,64);
  string_fromChar(&base,uri);

  string_resolveEnvVar(&base);

  currentDest = object_addDestination(currentObject,string_buffer(&base));
  if (!currentDest) {
    fprintf(out,"%s:%d: error: failed to add destination %s to object\n",currentFile->file_name,odl_yylineno,string_buffer(&base));
    SAH_TRACEZ_ERROR("odl","%s @ %d - failed to add destination %s to object",currentFile->file_name,odl_yylineno,string_buffer(&base));
    fatalError = 1;
  }
  string_cleanup(&base);
}

static void setTranslator(const char *functionName, const char *sofile) {
    if (verify) {
      return;
    }
    if(!functionName || !*functionName)  {
      fprintf(out,"%s:%d: error: Invalid function name\n",currentFile->file_name,odl_yylineno);
      SAH_TRACEZ_ERROR("odl","%s @ %d - Invalid function name",currentFile->file_name,odl_yylineno);
      return;
    }

    if (!datamodel_isAutoResolving(dm)) {
       return;
    }
    void *function = resolveFunction(sofile,functionName);
    if (!function) {
      SAH_TRACEZ_NOTICE("odl","%s @ %d - Unable to resolve function %s from %s",currentFile->file_name,odl_yylineno,functionName,(sofile && *sofile)?sofile:"this binary");
      return;
    }
    SAH_TRACEZ_INFO("odl","Handler function %s resolved",functionName);

    parameter_setTranslator(currentParameter,function);
}

static void addDestinationParameter(const char *client, const char *plugin) {
  if (verify) {
    return;
  }
  char *Name = string_resolveEnvVarChar(client?client:"");
  currentParameter = object_getParameter(currentObject,Name);
  if (!currentParameter) {
    SAH_TRACEZ_WARNING("odl","%s @ %d - parameter %s not found, skip mapping",currentFile->file_name,odl_yylineno,Name);
    free(Name);
    return;
  }
  if (!destination_addParameter(currentDest,Name,plugin)) {
    fprintf(out,"%s:%d: error: failed to add destination parameter %s for parameter %s\n",currentFile->file_name,odl_yylineno,plugin,Name);
    SAH_TRACEZ_ERROR("odl","%s @ %d - failed to add destination parameter %s for parameter %s",currentFile->file_name,odl_yylineno,plugin,Name);
    fatalError = 1;
    free(Name);
    return;
  }
  parameter_addMap(currentParameter,plugin,NULL);
  free(Name);
}

static void addDestinationFunction(const char *functionName, const char *destName) {
  if (verify) {
    return;
  }
  function_t *func = object_getFunction(currentObject,functionName);

  if (func) {
    fprintf(out,"%s:%d: warning: function %s already exists, skip mapping\n",currentFile->file_name,odl_yylineno,functionName);
    SAH_TRACEZ_WARNING("odl","%s @ %d - function %s already exists, skip mapping",currentFile->file_name,odl_yylineno,functionName);
    return;
  }

  if (!destination_addFunction(currentDest,functionName, destName)) {
    fprintf(out,"%s:%d: error: failed to add destination function %s\n",currentFile->file_name,odl_yylineno,functionName);
    SAH_TRACEZ_ERROR("odl","%s @ %d - failed to add destination function %s",currentFile->file_name,odl_yylineno,functionName);
    fatalError = 1;
    return;
  }
}

static uint32_t decode_parameter_attributes(int attributes) {
    uint32_t decoded_attributes = parameter_attr_default;

    if(attributes & attr_readonly)         decoded_attributes |= parameter_attr_read_only;
    if(attributes & attr_persistent)       decoded_attributes |= parameter_attr_persistent;
    if(attributes & attr_templateonly)     decoded_attributes |= parameter_attr_template_only;
    if(attributes & attr_volatile)         decoded_attributes |= parameter_attr_volatile;
    if(attributes & attr_upc)              decoded_attributes |= parameter_attr_persistent|parameter_attr_upc;
    if(attributes & attr_usersetting)      decoded_attributes |= parameter_attr_persistent|parameter_attr_upc|parameter_attr_upc_usersetting;
    if(attributes & attr_upc_overwrite)    decoded_attributes |= parameter_attr_upc_overwrite;
    if(attributes & attr_key)              decoded_attributes |= parameter_attr_key;

    return decoded_attributes;
}

static parameter_type_t decode_parameter_type(int type) {
    parameter_type_t decoded_type = parameter_type_unknown;

    switch(type) {
        default:
        case type_list:
        case type_unknown:
        case type_variant:
        case type_file_descriptor:
        case type_byte_array:
        case type_double:
            fprintf(out,"%s:%d: error: Wrong parameter type\n",currentFile->file_name,odl_yylineno);
            SAH_TRACEZ_ERROR("odl","%s @ %d - Wrong parameter type",currentFile->file_name,odl_yylineno);
            decoded_type = parameter_type_unknown;
        break;
        case type_string:
            decoded_type = parameter_type_string;
        break;
        case type_int8:
            decoded_type = parameter_type_int8;
        break;
        case type_int16:
            decoded_type = parameter_type_int16;
        break;
        case type_int32:
            decoded_type = parameter_type_int32;
        break;
        case type_int64:
            decoded_type = parameter_type_int64;
        break;
        case type_uint8:
            decoded_type = parameter_type_uint8;
        break;
        case type_uint16:
            decoded_type = parameter_type_uint16;
        break;
        case type_uint32:
            decoded_type = parameter_type_uint32;
        break;
        case type_uint64:
            decoded_type = parameter_type_uint64;
        break;
        case type_bool:
            decoded_type = parameter_type_bool;
        break;
        case type_date_time:
            decoded_type = parameter_type_date_time;
        break;
        case type_reference:
            decoded_type = parameter_type_reference;
        break;
    }

    return decoded_type;
}

static void createFunction(const char *name, uint32_t attribute,const function_type_t type) {
  if (verify || error_depth) {
     *definitions_available = true;
     return;
  }
  currentFunction = function_create(currentObject,name,type,attribute);
  if(!currentFunction) {
    currentFunction = object_getFunction(currentObject,name);
  }
  if(!currentFunction) {
    fprintf(out,"%s:%d: error: Cannot create function %s\n",currentFile->file_name,odl_yylineno, name);
    SAH_TRACEZ_ERROR("odl","%s @ %d - Cannot create function %s",currentFile->file_name,odl_yylineno,name);
    fatalError = 1;
#ifdef PCB_HELP_SUPPORT
    string_clear(&description);
#endif
    return;
  } else {
#ifdef PCB_HELP_SUPPORT
    const char *desc = getenv("PCB_DESCRIPTION")?getenv("PCB_DESCRIPTION"):"enabled";
    if (strcmp(desc,"enabled") == 0) {
        if (!string_isEmpty(&description)) {
          string_replaceChar(&description,"\"","'");
          string_appendFormat(&description,"\\n@defined %s:%d",currentFile->file_name,odl_yylineno);
        } else {
          string_fromChar(&description,"@defined ");
          string_appendFormat(&description,"%s:%d",currentFile->file_name,odl_yylineno);
        }
        function_setDescription(currentFunction,string_buffer(&description));
    }
    string_clear(&description);
#endif
  }


  datamodel_t *dm = pcb_datamodel(pcb);
  if (!datamodel_isAutoResolving(dm)) {
    return;
  }

  // resolving functions logic:
  // try in the following order
  // 1.__objectname_function
  // 2 objectname_function
  // 3 __function
  // 4 function

  char buf[512] = "";
  void *fp = NULL;
  string_t objectname;
  string_t functionname;
  string_initialize(&objectname, 256);
  string_initialize(&functionname, 256);
  string_fromChar(&objectname,object_name(currentObject,0));
  string_fromChar(&functionname,name);
  string_replaceChar(&objectname,"-","_");
  string_replaceChar(&functionname,"-","_");

  // 1 __objectname_function
  sprintf(buf,"__%s_%s",string_safeBuffer(&objectname),string_safeBuffer(&functionname));
  SAH_TRACEZ_INFO("odl","Try to resolve function name [%s]",buf);
  fp = resolveFunction(NULL,buf);
  if (fp) {
    SAH_TRACEZ_INFO("odl","Function resolved: [%s]",buf);
    function_setHandler(currentFunction,fp);
    string_cleanup(&functionname);
    string_cleanup(&objectname);
    return;
  }

  // 2 objectname_function
  sprintf(buf,"%s_%s",string_safeBuffer(&objectname),string_safeBuffer(&functionname));
  SAH_TRACEZ_INFO("odl","Try to resolve function name [%s]",buf);
  fp = resolveFunction(NULL,buf);
  if (fp) {
    SAH_TRACEZ_INFO("odl","Function resolved: [%s]",buf);
    function_setHandler(currentFunction,fp);
    string_cleanup(&functionname);
    string_cleanup(&objectname);
    return;
  }

  // 3 objectname_function
  sprintf(buf,"__%s",string_safeBuffer(&functionname));
  SAH_TRACEZ_INFO("odl","Try to resolve function name [%s]",buf);
  fp = resolveFunction(NULL,buf);
  if (fp) {
    SAH_TRACEZ_INFO("odl","Function resolved: [%s]",buf);
    function_setHandler(currentFunction,fp);
    string_cleanup(&functionname);
    string_cleanup(&objectname);
    return;
  }

  // 4 objectname_function
  fp = resolveFunction(NULL,string_safeBuffer(&functionname));
  SAH_TRACEZ_INFO("odl","Try to resolve function name [%s]",name);
  if (fp) {
    SAH_TRACEZ_INFO("odl","Function resolved: [%s]",name);
    function_setHandler(currentFunction,fp);
    string_cleanup(&functionname);
    string_cleanup(&objectname);
    return;
  }
  string_cleanup(&functionname);
  string_cleanup(&objectname);
}

static void createCustomFunction(const char *name, uint32_t attribute,const char *type) {
  if (verify || error_depth) {
     *definitions_available = true;
     return;
  }
  currentFunction = function_createCustomType(currentObject,name,type,attribute);
  if(!currentFunction) {
    currentFunction = object_getFunction(currentObject,name);
  }
  if(!currentFunction) {
    fprintf(out,"%s:%d: error: Cannot create function %s\n",currentFile->file_name,odl_yylineno, name);
    SAH_TRACEZ_ERROR("odl","%s @ %d - Cannot create function %s",currentFile->file_name,odl_yylineno,name);
    fatalError = 1;
#ifdef PCB_HELP_SUPPORT
    string_clear(&description);
#endif
    return;
  } else {
#ifdef PCB_HELP_SUPPORT
    const char *desc = getenv("PCB_DESCRIPTION")?getenv("PCB_DESCRIPTION"):"enabled";
    if (strcmp(desc,"enabled") == 0) {
        if (!string_isEmpty(&description)) {
          string_replaceChar(&description,"\"","'");
          string_appendFormat(&description,"\\n@defined %s:%d",currentFile->file_name,odl_yylineno);
        } else {
          string_fromChar(&description,"@defined ");
          string_appendFormat(&description,"%s:%d",currentFile->file_name,odl_yylineno);
        }
        function_setDescription(currentFunction,string_buffer(&description));
    }
    string_clear(&description);
#endif
  }


  datamodel_t *dm = pcb_datamodel(pcb);
  if (!datamodel_isAutoResolving(dm)) {
    return;
  }

  char buf[512] = "";
  void *fp = NULL;

  // 1 __objectname_function
  sprintf(buf,"__%s_%s",object_name(currentObject,0),name);
  SAH_TRACEZ_INFO("odl","Try to resolve function name [%s]",buf);
  fp = resolveFunction(NULL,buf);
  if (fp) {
    SAH_TRACEZ_INFO("odl","Function resolved: [%s]",buf);
    function_setHandler(currentFunction,fp);
    return;
  }

  // 2 objectname_function
  sprintf(buf,"%s_%s",object_name(currentObject,0),name);
  SAH_TRACEZ_INFO("odl","Try to resolve function name [%s]",buf);
  fp = resolveFunction(NULL,buf);
  if (fp) {
    SAH_TRACEZ_INFO("odl","Function resolved: [%s]",buf);
    function_setHandler(currentFunction,fp);
    return;
  }

  // 3 objectname_function
  sprintf(buf,"__%s",name);
  SAH_TRACEZ_INFO("odl","Try to resolve function name [%s]",buf);
  fp = resolveFunction(NULL,buf);
  if (fp) {
    SAH_TRACEZ_INFO("odl","Function resolved: [%s]",buf);
    function_setHandler(currentFunction,fp);
    return;
  }

  // 4 objectname_function
  fp = resolveFunction(NULL,name);
  SAH_TRACEZ_INFO("odl","Try to resolve function name [%s]",name);
  if (fp) {
    SAH_TRACEZ_INFO("odl","Function resolved: [%s]",name);
    function_setHandler(currentFunction,fp);
    return;
  }
  fprintf(out,"%s:%d: warning: Unable to resolve function %s\n",currentFile->file_name,odl_yylineno,name);
  SAH_TRACEZ_WARNING("odl","%s @ %d - Unable to resolve function %s",currentFile->file_name,odl_yylineno,name);
}

static void selectFunction(const char *name) {
  char *Name = string_resolveEnvVarChar(name?name:"");
  currentFunction = object_getFunction(currentObject,Name);
  if(!currentFunction) {
    fprintf(out,"%s:%d: warning: Function %s not found\n",currentFile->file_name,odl_yylineno, Name);
    SAH_TRACEZ_WARNING("odl","%s @ %d - Function %s not found",currentFile->file_name,odl_yylineno,Name);
  }
  free(Name);
}

static uint32_t decode_function_attributes(int attributes) {
    uint32_t decoded_attributes = function_attr_default;

    if(attributes & attr_templateonly)     decoded_attributes |= function_attr_template_only;
    if(attributes & attr_message)          decoded_attributes |= function_attr_message;
    if(attributes & attr_async)            decoded_attributes |= function_attr_async;

    return decoded_attributes;
}

static function_type_t decode_function_type(int type) {
    function_type_t decoded_type = function_type_void;

    switch(type) {
        default:
        case type_unknown:
            decoded_type = function_type_void;
        break;
        case type_reference:
        case type_string:
            decoded_type =  function_type_string;
        break;
        case type_int8:
            decoded_type =  function_type_int8;
        break;
        case type_int16:
            decoded_type =  function_type_int16;
        break;
        case type_int32:
            decoded_type =  function_type_int32;
        break;
        case type_int64:
            decoded_type =  function_type_int64;
        break;
        case type_uint8:
            decoded_type =  function_type_uint8;
        break;
        case type_uint16:
            decoded_type =  function_type_uint16;
        break;
        case type_uint32:
            decoded_type =  function_type_uint32;
        break;
        case type_uint64:
            decoded_type =  function_type_uint64;
        break;
        case type_bool:
            decoded_type =  function_type_bool;
        break;
        case type_date_time:
            decoded_type =  function_type_date_time;
        break;
        case type_list:
            decoded_type =  function_type_list;
        break;
        case type_file_descriptor:
            decoded_type =  function_type_file_descriptor;
        break;
        case type_byte_array:
            decoded_type =  function_type_byte_array;
        break;
        case type_variant:
            decoded_type =  function_type_variant;
        break;
        case type_double:
            decoded_type =  function_type_double;
        break;
    }

    return decoded_type;
}

static void addArgument(const char *name, uint32_t attribute,const argument_type_t type) {
    if (verify) {
       return;
    }
    function_argument_t *arg = argument_create(currentFunction,name,type,attribute);
    if (!arg) {
        arg = function_getArgument(currentFunction, name);
    }
    if (!arg) {
      fprintf(out,"%s:%d: error: Cannot create argument %s\n",currentFile->file_name,odl_yylineno, name);
      SAH_TRACEZ_ERROR("odl","%s @ %d - Cannot create argument %s",currentFile->file_name,odl_yylineno,name);
      fatalError = 1;
    }
}

static void addArgumentCustom(const char *name, uint32_t attribute,const char *type) {
    if (verify) {
        return;
    }

    function_argument_t *arg = argument_createCustomType(currentFunction,name,type,attribute);
    if (!arg) {
        arg = function_getArgument(currentFunction, name);
    }
    if (!arg) {
        fprintf(out,"%s:%d: error: Cannot create argument %s\n",currentFile->file_name,odl_yylineno, name);
        SAH_TRACEZ_ERROR("odl","%s @ %d - Cannot create argument %s",currentFile->file_name,odl_yylineno,name);
        fatalError = 1;
    }
}

static uint32_t decode_argument_attributes(int attributes) {
    uint32_t decoded_attributes = function_attr_default;

    if(attributes & attr_in)            decoded_attributes |= argument_attr_in;
    if(attributes & attr_out)           decoded_attributes |= argument_attr_out;
    if(attributes & attr_mandatory)     decoded_attributes |= argument_attr_mandatory;

    return decoded_attributes;
}

static argument_type_t decode_argument_type(int type) {
    argument_type_t decoded_type = argument_type_unknown;

    switch(type) {
        default:
        case type_unknown:
            fprintf(out,"%s:%d: error: Wrong argument type\n",currentFile->file_name,odl_yylineno);
            SAH_TRACEZ_ERROR("odl","%s @ %d - Wrong argument type",currentFile->file_name,odl_yylineno);
            decoded_type = argument_type_unknown;
        break;
        case type_reference:
        case type_string:
            decoded_type =  argument_type_string;
        break;
        case type_int8:
            decoded_type =  argument_type_int8;
        break;
        case type_int16:
            decoded_type =  argument_type_int16;
        break;
        case type_int32:
            decoded_type =  argument_type_int32;
        break;
        case type_int64:
            decoded_type =  argument_type_int64;
        break;
        case type_uint8:
            decoded_type =  argument_type_uint8;
        break;
        case type_uint16:
            decoded_type =  argument_type_uint16;
        break;
        case type_uint32:
            decoded_type =  argument_type_uint32;
        break;
        case type_uint64:
            decoded_type =  argument_type_uint64;
        break;
        case type_bool:
            decoded_type =  argument_type_bool;
        break;
        case type_date_time:
            decoded_type =  argument_type_date_time;
        break;
        case type_list:
            decoded_type = argument_type_list;
        break;
        case type_file_descriptor:
            decoded_type = argument_type_file_descriptor;
        break;
        case type_byte_array:
            decoded_type = argument_type_byte_array;
        break;
        case type_variant:
            decoded_type = argument_type_variant;
        break;
        case type_double:
            decoded_type = argument_type_double;
        break;
    }

    return decoded_type;
}

static void setDefaultValue(const char *value) {
    if (verify) {
        return;
    }
    if(!currentParameter) {
      SAH_TRACEZ_WARNING("odl","%s @ %d - Default value not on a valid place in the odl file",currentFile->file_name,odl_yylineno);
      return;
    }

    string_t data;
    string_initialize(&data,0);
    char *resolved = string_resolveEnvVarChar(value?value:"");
    string_fromChar(&data,resolved);
    free(resolved);
    string_replaceChar(&data,"\\\"","\"");
    string_replaceChar(&data,"\\$","$");
    string_replaceChar(&data,"\\(","(");
    string_replaceChar(&data,"\\)",")");
    string_replaceChar(&data,"\\\\","\\");

    if(!parameter_setFromChar(currentParameter,string_buffer(&data))) {
       fprintf(out,"%s:%d: error: Cannot set default value for parameter\n",currentFile->file_name,odl_yylineno);
       SAH_TRACEZ_ERROR("odl","%s @ %d - Cannot set default value for parameter",currentFile->file_name,odl_yylineno);
       fatalError = 1;
    }
    string_cleanup(&data);

#ifdef PCB_HELP_SUPPORT
 const char *desc = getenv("PCB_DESCRIPTION")?getenv("PCB_DESCRIPTION"):"enabled";
    if (strcmp(desc,"enabled") == 0) {
        string_t newd;
        string_initialize(&newd, 64);
        string_fromChar(&newd, parameter_getDescription(currentParameter));
        string_appendFormat(&newd, "\\n@assigned %s:%d",currentFile->file_name,odl_yylineno);
        parameter_setDescription(currentParameter, string_buffer(&newd));
        string_cleanup(&newd);
    }
    string_clear(&description);
#endif
}

static void setMinimumConstraint(const char *minValue) {
    if (verify) {
       return;
    }
    if(!currentParameter) {
      fprintf(out,"%s:%d: error: Constraint not on a valid place in the ol file\n",currentFile->file_name,odl_yylineno);
      SAH_TRACEZ_ERROR("odl","%s @ %d - Constraint not on a valid place in the ol file",currentFile->file_name,odl_yylineno);
      fatalError = 1;
    }

    variant_t myValue;
    variant_initialize(&myValue,variant_type_string);
    variant_setChar(&myValue, minValue);
    parameter_setValidator(currentParameter,param_validator_create_minimum(&myValue));
    variant_cleanup(&myValue);
}

static void setMaximumConstraint(const char *maxValue) {
    if (verify) {
       return;
    }
    if(!currentParameter) {
      fprintf(out,"%s:%d: error: Constraint not on a valid place in the odl filen",currentFile->file_name,odl_yylineno);
      SAH_TRACEZ_ERROR("odl","%s @ %d - Constraint not on a valid place in the odl file",currentFile->file_name,odl_yylineno);
      fatalError = 1;
    }

    variant_t myValue;
    variant_initialize(&myValue,variant_type_string);
    variant_setChar(&myValue, maxValue);
    parameter_setValidator(currentParameter,param_validator_create_maximum(&myValue));
    variant_cleanup(&myValue);
}

static void setRangeConstraint(const char *minValue,const char *maxValue) {
    if (verify) {
       return;
    }
    if(!currentParameter) {
      fprintf(out,"%s:%d: error: Constraint not on a valid place in the odl filen",currentFile->file_name,odl_yylineno);
      SAH_TRACEZ_ERROR("odl","%s @ %d - Constraint not on a valid place in the odl file",currentFile->file_name,odl_yylineno);
      fatalError = 1;
    }

    variant_t myValue1;
    variant_initialize(&myValue1,variant_type_string);
    variant_setChar(&myValue1, minValue);
    variant_t myValue2;
    variant_initialize(&myValue2,variant_type_string);
    variant_setChar(&myValue2, maxValue);

    parameter_setValidator(currentParameter,param_validator_create_range(&myValue1,&myValue2));
    variant_cleanup(&myValue1);
    variant_cleanup(&myValue2);
}

static void setEnumConstraint() {
    if (verify) {
       return;
    }
    parameter_setValidator(currentParameter,param_validator_create_enum(enumList));
}

static void initEnumValue(const char *value) {
  if (verify) {
     return;
  }
  enumList = calloc(1, sizeof(variant_list_t));
  variant_list_initialize(enumList);
  addEnumValue(value);
}

static void addEnumValue(const char *value) {
  if (verify) {
     return;
  }
  variant_t myVariant;
  variant_initialize(&myVariant, variant_type_string);
  char *resolved = string_resolveEnvVarChar(value?value:"");
  variant_setChar(&myVariant,resolved);
  variant_list_iterator_t *temp;
  temp = variant_list_iterator_create(&myVariant);
  variant_list_append(enumList, temp);
  variant_cleanup(&myVariant);
  free(resolved);
}

static void *openLibrary(const char *sofile, int dlflags) {
  void *handle = dlopen(sofile, dlflags);
  if (handle) {
    SAH_TRACEZ_INFO("load","Library %s loaded",sofile?sofile:"self");
  } else {
    const char *error = dlerror();
    if (error && !strstr(error, strerror(ENOENT))) {
        //The shared library file exists but can not be loaded
        fprintf(out,"%s:%d: error: Failed to open %s (error = %s)\n",currentFile->file_name,odl_yylineno,sofile,error);
        SAH_TRACEZ_ERROR("odl","%s @ %d - Failed to open %s (error = %s)",currentFile->file_name,odl_yylineno,sofile,error);
    }
    else {
        //The shared library file does not exist
        SAH_TRACEZ_INFO("odl","%s @ %d - Failed to open %s (error = %s)",currentFile->file_name,odl_yylineno,sofile,error);
    }
  }

  return handle;
}

static void *loadLibrary(const char *filename, bool global) {
  void *handle = NULL;
  char *sofile = NULL;
  const char *plugin_path_default = CONFIG_PCB_DEFAULT_PLUGIN_PATH;
  library_t *lib = NULL;
  bool loaded = false;
  llist_iterator_t *it = NULL;
  int dlflags = RTLD_NOW;

  if (global) {
    dlflags |= RTLD_GLOBAL;
  }

  if (!datamodel_isAutoResolving(dm)) {
    SAH_TRACEZ_WARNING("odl","Auto function resolving is turned off");

    return NULL;
  }

  SAH_TRACEZ_INFO("load","Search and load %s",filename);
  /* search shared objects in the following order
        0. starts with "/" => absolute path
        else
        1. input_dir + filename
        2. filename
        3. use "/usr/lib/" + name + filename
        4. use PCB_PLUGIN_PATH + filename
  */
  if (filename && *filename) {
    const char *plugin_path = getenv("PCB_PLUGIN_PATH");
    if (!plugin_path) {
      plugin_path = plugin_path_default;
    }
    sofile = (char *)calloc(1,1024);
    if (!sofile) {
      SAH_TRACEZ_ERROR("odl","Failed to allocate memory");
      return NULL;
    }

    if (filename[0] == '/') {
        SAH_TRACEZ_INFO("odl","Try to load %s",filename);
        strcpy(sofile,filename);
        handle = openLibrary(sofile, dlflags);
        if (handle) {
          goto library_found;
        }
    } else {
        // 1 input dir
        if (input_dir && *input_dir) {
          strcpy(sofile,input_dir);
          strcat(sofile,"/");
          strcat(sofile,filename);
          SAH_TRACEZ_INFO("odl","Try to load %s",sofile);
          handle = openLibrary(sofile, dlflags);
          if (handle) {
            goto library_found;
          }
        }

        // 2. filename
        strcpy(sofile,filename);
        SAH_TRACEZ_INFO("odl","Try to load %s",sofile);
        handle = openLibrary(sofile, dlflags);
        if (handle) {
          goto library_found;
        }
        // 3. /usr/lib/ + name + filename
        if (pcb_name && *pcb_name) {
          strcpy(sofile,"/usr/lib/");
          strcat(sofile,pcb_name);
          strcat(sofile,"/");
          strcat(sofile,filename);
          SAH_TRACEZ_INFO("odl","Try to load %s",sofile);
          handle = openLibrary(sofile, dlflags);
          if (handle) {
            goto library_found;
          }
        }

        // 4. PCB_PLUGIN_PATH + filename
        strcpy(sofile,plugin_path);
        strcat(sofile,"/");
        strcat(sofile,filename);
        SAH_TRACEZ_INFO("odl","Try to load %s",sofile);
        handle = openLibrary(sofile, dlflags);
        if (handle) {
          goto library_found;
        }
     }
  } else {
    if (!defaultLibraryHandle) {
      handle = openLibrary(NULL, RTLD_NOW); // open myself
    } else {
      return defaultLibraryHandle;
    }
  }

  if (!handle) {
    if (sofile) {
      fprintf(out,"%s:%d: error: Failed to open %s (error = %s)\n",currentFile->file_name,odl_yylineno,sofile, dlerror());
      SAH_TRACEZ_WARNING("odl","%s @ %d - Failed to open %s (error = %s)",currentFile->file_name,odl_yylineno,sofile, dlerror());
    }
  } else {
    if (sofile) {
      SAH_TRACEZ_INFO("odl","Loaded library %s", sofile);
    }
  }

  if (!handle) {
    free(sofile);
    return NULL;
  }

library_found:
  // dlopen will return the same handle if we try to open the same lib again
  // but still we need to close the handle, to decrease the reference count
  // check if the handle is already available in the list of loaded libraries
  SAH_TRACEZ_INFO("load","Library %s loaded, Check that library was already loaded before (current handle = %p)",filename,handle);
  llist_for_each(it,&libraries) {
      lib = llist_item_data(it,library_t,it);
      SAH_TRACEZ_INFO("load","Current handle = %p, library handle = %p",handle,lib->handle);
      if (lib->handle == handle ||
          (sofile && strcmp(sofile,lib->filename)) == 0) {
          SAH_TRACEZ_INFO("load","Library was already loaded, close handle, and continue");
          dlclose(handle);
          loaded = true;
          handle = lib->handle;
          break;
      }
  }

  if (!loaded) {
    lib = (library_t *)calloc(1,sizeof(library_t));
    if (!lib) {
      SAH_TRACEZ_ERROR("odl","%s",error_string(pcb_error));
      dlclose(handle);
      free(sofile);
      return NULL;
    }
    pcb_plugin_enter_handler pcb_plugin_enter = dlsym(handle, "pcb_plugin_enter");
    if (pcb_plugin_enter) {
        pcb_plugin_enter(pcb);
    } else {
        // always call dlerror, otherwise the next dlsym will fail also
        const char *error = dlerror();
        SAH_TRACEZ_INFO("odl","pcb_plugin_enter not found %s",error);
    }
    lib->handle = handle;
    lib->initialized = false;
    lib->filename = sofile?strdup(sofile):NULL;
    SAH_TRACEZ_INFO("load","add library to list");
    llist_append(&libraries,&lib->it);
  }

  free(sofile);
  return handle;
}

static void *resolveFunction(const char *filename,const char *functionName)
{
  void *handle = NULL;
  void *fp     = NULL;

  if (!datamodel_isAutoResolving(dm)) {
    return NULL;
  }

  if (filename) {
      SAH_TRACEZ_INFO("odl","load library %s",filename);
      handle = loadLibrary(filename, false);
      if (!handle) {
        fprintf(out,"%s:%d: warning: Failed to load %s (error=%s)\n",currentFile->file_name,odl_yylineno,filename?filename:"",dlerror());
        SAH_TRACEZ_INFO("odl","%s @ %d - Failed to load %s (error=%s)",currentFile->file_name,odl_yylineno,filename?filename:"",dlerror());
        return NULL;
      }
  } else if (currentFile->libraryHandle) {
    handle = currentFile->libraryHandle;
  } else {
    handle = defaultLibraryHandle;
  }

  fp = dlsym(handle, functionName);
  if (!fp) {
    // always call dlerror, otherwise the next dlsym will fail also
    const char *error = dlerror();
    SAH_TRACEZ_INFO("odl","symbol not found %s - %s",functionName,error);

    return NULL;
  }

  return fp;
}

static void setCustomConstraint(const char *functionName, const char *sofile) {
    if (verify) {
       return;
    }
    if(!functionName || !*functionName)  {
      fprintf(out,"%s:%d: error: Invalid function name\n",currentFile->file_name,odl_yylineno);
      SAH_TRACEZ_ERROR("odl","%s @ %d - Invalid function name",currentFile->file_name,odl_yylineno);
      return;
    }

    void *function = resolveFunction(sofile,functionName);
    if (!function) {
      datamodel_t *dm = pcb_datamodel(pcb);
      if (datamodel_isAutoResolving(dm)) {
        fprintf(out,"%s:%d: warning: Unable to resolve function %s from %s\n",currentFile->file_name,odl_yylineno,functionName,sofile?sofile:"this binary");
        SAH_TRACEZ_WARNING("odl","%s @ %d - Unable to resolve function %s from %s",currentFile->file_name,odl_yylineno,functionName,sofile?sofile:"this binary");
      }
      return;
    }

    if(currentParameter) {
      // add constraint to parameter
      parameter_setValidator(currentParameter,param_validator_create_custom(function,NULL));
    } else {
      // add constraint to object
      object_setValidator(currentObject,object_validator_create(function,NULL));
    }
}

static void addRuleRenameModifier(const char *functionName, const char *sofile) {
    if (verify) {
      return;
    }
    if(!functionName || !*functionName)  {
      fprintf(out,"%s:%d: error: Invalid function name\n",currentFile->file_name,odl_yylineno);
      SAH_TRACEZ_ERROR("odl","%s @ %d - Invalid function name",currentFile->file_name,odl_yylineno);
      return;
    }

    void *function = resolveFunction(sofile,functionName);
    if (!function) {
      datamodel_t *dm = pcb_datamodel(pcb);
      if (datamodel_isAutoResolving(dm)) {
        SAH_TRACEZ_NOTICE("odl","%s @ %d - Unable to resolve function %s from %s",currentFile->file_name,odl_yylineno,functionName,sofile?sofile:"this binary");
      }
      return;
    }

    if(currentRule) {
        rule_addModifierRename(currentRule,function);
    }
}

static void addRuleTranslateModifier(const char *functionName, const char *sofile) {
    if (verify) {
      return;
    }
    if(!functionName || !*functionName)  {
      fprintf(out,"%s:%d: error: Invalid function name\n",currentFile->file_name,odl_yylineno);
      SAH_TRACEZ_ERROR("odl","%s @ %d - Invalid function name",currentFile->file_name,odl_yylineno);
      return;
    }

    void *function = resolveFunction(sofile,functionName);
    if (!function) {
      datamodel_t *dm = pcb_datamodel(pcb);
      if (datamodel_isAutoResolving(dm)) {
        fprintf(out,"%s:%d: warning: Unable to resolve function %s from %s\n",currentFile->file_name,odl_yylineno,functionName,sofile?sofile:"this binary");
        SAH_TRACEZ_WARNING("odl","%s @ %d - Unable to resolve function %s from %s",currentFile->file_name,odl_yylineno,functionName,sofile?sofile:"this binary");
      }
      return;
    }

    if(currentRule) {
        rule_addModifierTranslate(currentRule,function);
    }
}

static void addRuleCastModifier(const char *functionName, const char *sofile) {
    if (verify) {
      return;
    }
    if(!functionName || !*functionName)  {
      fprintf(out,"%s:%d: error: Invalid function name\n",currentFile->file_name,odl_yylineno);
      SAH_TRACEZ_ERROR("odl","%s @ %d - Invalid function name",currentFile->file_name,odl_yylineno);
      return;
    }

    void *function = resolveFunction(sofile,functionName);
    if (!function) {
      datamodel_t *dm = pcb_datamodel(pcb);
      if (datamodel_isAutoResolving(dm)) {
        fprintf(out,"%s:%d: warning: Unable to resolve function %s from %s\n",currentFile->file_name,odl_yylineno,functionName,sofile?sofile:"this binary");
        SAH_TRACEZ_WARNING("odl","%s @ %d - Unable to resolve function %s from %s",currentFile->file_name,odl_yylineno,functionName,sofile?sofile:"this binary");
      }
      return;
    }

    if(currentRule) {
        rule_addModifierCast(currentRule,function);
    }
}

static void setCallbackFunction(int keyword,const char *functionName, const char *sofile) {
    if (verify) {
       return;
    }
    SAH_TRACEZ_IN("odl");
    bool result = true;

    if(!functionName || !*functionName)  {
      fprintf(out,"%s:%d: error: Invalid function name\n",currentFile->file_name,odl_yylineno);
      SAH_TRACEZ_ERROR("odl","%s @ %d - Invalid function name",currentFile->file_name,odl_yylineno);
      return;
    }

    void *function = resolveFunction(sofile,functionName);
    if (!function) {
      datamodel_t *dm = pcb_datamodel(pcb);
      if (datamodel_isAutoResolving(dm)) {
        fprintf(out,"%s:%d: warning: Unable to resolve function %s from %s\n",currentFile->file_name,odl_yylineno,functionName,sofile?sofile:"this binary");
        SAH_TRACEZ_WARNING("odl","%s @ %d - Unable to resolve function %s from %s",currentFile->file_name,odl_yylineno,functionName,sofile?sofile:"this binary");
      }
      return;
    }
    SAH_TRACEZ_INFO("odl","Handler function %s resolved",functionName);

    if(currentParameter) {
      switch(keyword) {
        case keyword_read:
            parameter_setReadHandler(currentParameter, function);
        break;
        case keyword_write:
            parameter_setWriteHandler(currentParameter, function);
        break;
        case keyword_prewrite:
            parameter_setPrewriteHandler(currentParameter, function);
        break;
        default:
            SAH_TRACEZ_WARNING("odl","%s @ %d - Invalid handler type specified for parameter",currentFile->file_name, odl_yylineno);
            result = false;
        break;
      }
    } else {
      switch(keyword) {
        case keyword_add:
            object_setInstanceAddHandler(currentObject, function);
        break;
        case keyword_delete:
            object_setInstanceDelHandler(currentObject, function);
        break;
        case keyword_read:
            object_setReadHandler(currentObject, function);
        break;
        case keyword_write:
            object_setWriteHandler(currentObject, function);
        break;
        case keyword_mib_add:
            object_setMibAddHandler(currentObject, function);
        break;
        case keyword_mib_del:
            object_setMidDelHandler(currentObject, function);
        break;
        default:
            SAH_TRACEZ_WARNING("odl","%s @ %d - Invalid handler type specified for object",currentFile->file_name, odl_yylineno);
            result = false;
        break;
      }
    }

    if(!result) {
      fprintf(out,"%s:%d error:: Error adding callback function %s\n",currentFile->file_name,odl_yylineno,functionName);
      SAH_TRACEZ_ERROR("odl","%s @ %d - Error adding callback function %s",currentFile->file_name,odl_yylineno,functionName);
    }

}

static void validateParameter(parameter_t *parameter) {
    if (verify) {
        return;
    }
    if (!parameter_validate(parameter)) {
        fprintf(out,"%s:%d: error: Parameter %s of object %s\n",currentFile->file_name,odl_yylineno,parameter_name(parameter),object_name(parameter_owner(parameter),0));
        SAH_TRACEZ_ERROR("odl","%s @ %d - Paremeter %s of object %s",currentFile->file_name,odl_yylineno,parameter_name(parameter),object_name(parameter_owner(parameter),0));
        fprintf(out,"\tParameter validation failed 0x%.8X : %s\n",pcb_error,error_string(pcb_error));
        SAH_TRACEZ_ERROR("odl","Parameter validation failed 0x%.8X : %s",pcb_error,error_string(pcb_error));
        fatalError = 1;
    }
}

static void addCustomType(const char *name) {
    if (!verify) {
        // only during verification/documentation process custom types are store
        // they have no real functionality.
        return;
    }
    variant_map_addMapMove(custom_types, name, NULL);
    current_type = variant_map_da_findMap(custom_types, name);

    variant_map_addMapMove(current_type, "members", NULL);
    variant_map_addMapMove(current_type, "descriptions", NULL);

#ifdef PCB_HELP_SUPPORT
    const char *desc = getenv("PCB_DESCRIPTION")?getenv("PCB_DESCRIPTION"):"enabled";
    if (strcmp(desc,"enabled") == 0) {
        if (!string_isEmpty(&description)) {
            string_replaceChar(&description,"\"","'");
            string_appendFormat(&description,"\\n@defined %s:%d",currentFile->file_name,odl_yylineno);
        } else {
          string_fromChar(&description,"@defined ");
          string_appendFormat(&description,"%s:%d",currentFile->file_name,odl_yylineno);
        }

        variant_map_addString(current_type, "description", &description);
    }
    string_clear(&description);
#endif
}

static void addCustomTypeMember(const char *name, uint32_t type, const char *custom_type)
{
    if (!verify) {
        // only during verification/documentation process custom types are store
        // they have no real functionality.
        return;
    }

    variant_map_t *types = variant_map_da_findMap(current_type, "members");
    if (custom_type) {
        if (!variant_map_contains(custom_types, custom_type)) {
            SAH_TRACEZ_WARNING("odl","Custom type %s not known, ignoring definition %s @ %d", custom_type, currentFile->file_name,odl_yylineno);
            fprintf(out,"\tUnkown custom type %s - ignoring definition %s @ %d\n",custom_type, currentFile->file_name,odl_yylineno);
        } else {
            variant_map_addChar(types, name, custom_type);
        }
    } else {
        variant_map_addUInt32(types, name, type);
    }

#ifdef PCB_HELP_SUPPORT
    variant_map_t *descriptions = variant_map_da_findMap(current_type, "descriptions");
    const char *desc = getenv("PCB_DESCRIPTION")?getenv("PCB_DESCRIPTION"):"enabled";
    if (strcmp(desc,"enabled") == 0) {
        if (!string_isEmpty(&description)) {
            string_replaceChar(&description,"\"","'");
            string_appendFormat(&description,"\\n@defined %s:%d",currentFile->file_name,odl_yylineno);
        } else {
          string_fromChar(&description,"@defined ");
          string_appendFormat(&description,"%s:%d",currentFile->file_name,odl_yylineno);
        }

        variant_map_addString(descriptions, name, &description);
    }
    string_clear(&description);
#endif
}
static void setDefaultLibrary(const char *filename, const char *flags) {
    bool global = false;
    if (verify || !datamodel_isAutoResolving(dm)) {
        return;
    }

    if (flags && *flags) {
        if (!strcmp(flags, "global")) {
            global = true;
        }
        else {
            fprintf(out,"Unknown library load flags %s - ignoring: %s @ %d\n",flags,currentFile->file_name,odl_yylineno);
            SAH_TRACEZ_WARNING("odl","Unknown library load flags %s - ignoring: %s @ %d",flags,currentFile->file_name,odl_yylineno);
        }
    }
    void *handle = loadLibrary(filename, global);

    if (!handle) {
        fprintf(out,"%s:%d: error: Failed to load %s (error=%s)\n",currentFile->file_name,odl_yylineno,filename?filename:"",dlerror());
        SAH_TRACEZ_WARNING("odl","%s @ %d - Failed to load %s (error=%s)",currentFile->file_name,odl_yylineno,filename?filename:"",dlerror());
        return;
    }

    // set the global library
    if (llist_size(&odl_file_stack) == 1) {
        defaultLibraryHandle = handle;
    }

    currentFile->libraryHandle = handle;
}

#ifdef CONFIG_PCB_ACL_USERMNGT
static void setAcl(const char *value, uint16_t flags) {
    if (verify) {
        return;
    }
    uint32_t id = 0;
    if (!value) {
        id = UINT32_MAX;
    } else if (flags & acl_group_id) {
        const usermngt_group_t *gr = usermngt_groupFindByName(value);
        if (gr == NULL) {
            SAH_TRACEZ_NOTICE("odl","unable to fetch id for group %s", value);
            return;
        }
        SAH_TRACEZ_INFO("odl","Group %s: id = %d",value, usermngt_groupID(gr));
        id = usermngt_groupID(gr);
    } else {
        const usermngt_user_t *pw = usermngt_userFindByName(value);
        if (pw == NULL) {
            SAH_TRACEZ_NOTICE("odl","unable to fetch id for user %s", value);
            return;
        }
        SAH_TRACEZ_INFO("odl","User %s: id = %d",value,usermngt_userID(pw));
        id = usermngt_userID(pw);
    }

    if (currentParameter) {
        parameter_aclSet(currentParameter,id,flags);
    } else if (currentFunction) {
        function_aclSet(currentFunction,id,flags);
    } else {
        object_aclSet(currentObject,id,flags);
    }
    return;
}
#else
static void setAcl(const char *value, uint16_t flags) {
    if (verify) {
        return;
    }
    uint32_t id = 0;
    if (!value) {
        id = UINT32_MAX;
    } else if (flags & acl_group_id) {
        struct group *gr = getgrnam(value);
        if (gr == NULL) {
            SAH_TRACEZ_WARNING("odl","unable to fetch id for group %s", value);
            return;
        }
        SAH_TRACEZ_INFO("odl","Group %s: id = %d",value,gr->gr_gid);
        id = gr->gr_gid;
    } else {
        struct passwd *pw = getpwnam(value);
        if (pw == NULL) {
            SAH_TRACEZ_WARNING("odl","unable to fetch id for user %s", value);
            return;
        }
        SAH_TRACEZ_INFO("odl","User %s: id = %d",value,pw->pw_uid);
        id = pw->pw_uid;
    }

    if (currentParameter) {
        parameter_aclSet(currentParameter,id,flags);
    } else if (currentFunction) {
        function_aclSet(currentFunction,id,flags);
    } else {
        object_aclSet(currentObject,id,flags);
    }
    return;
}
#endif

static void setInstanceCounter(const char *parameterName) {
    if (verify) {
        return;
    }
    char *Name = string_resolveEnvVarChar(parameterName?parameterName:"");
    if (object_setInstanceCounter(currentObject, Name) == NULL) {
        fprintf(out,"%s:%d: error: unable to add instance counter %s\n",currentFile->file_name,odl_yylineno,Name);
        SAH_TRACEZ_ERROR("odl","%s @ %d - unable to add instance counter %s",currentFile->file_name,odl_yylineno,Name);
        fatalError = 1;
    }
    free(Name);
}

int odl_yyerror(const char *msg)
{
  if (strstr(msg,"$end") == 0) {
    fprintf(out,"%s:%d: error: %s\n",currentFile->file_name,odl_yylineno,msg);
    SAH_TRACEZ_ERROR("odl","%s @ %d - %s",currentFile->file_name,odl_yylineno,msg);
    fatalError = 1;
  }
  return 0;
}

static odl_file_t *push_odl_file(const char *file_name) {
    odl_file_t *file = (odl_file_t *)calloc(1,sizeof(odl_file_t));
    if (!file_name) {
        SAH_TRACEZ_ERROR("odl","No more memory\n");
        return NULL;
    }

    file->file_name = strdup(file_name);
    file->linenr = odl_yylineno;
    if (currentFile)
        file->libraryHandle = currentFile->libraryHandle;
    odl_yylineno = 1;

    llist_prepend(&odl_file_stack,&file->it);
    currentFile = file;
    return file;
}

static void pop_odl_file() {
    llist_iterator_t *it = llist_takeFirst(&odl_file_stack);
    if (!it) {
        currentFile = NULL;
        return;
    }
    
    odl_file_t *file = llist_item_data(it,odl_file_t,it);
    odl_yylineno = file->linenr;

#ifdef PCB_HELP_SUPPORT
    const char *desc = getenv("PCB_DESCRIPTION")?getenv("PCB_DESCRIPTION"):"enabled";
    if (strcmp(desc,"enabled") == 0) {
         if (!string_isEmpty(&description)) {
             variant_map_addString(file_locations, file->file_name, &description);
         }
    }
    string_clear(&description);
#endif

    free(file->file_name);
    free(file);

    it = llist_first(&odl_file_stack);
    if (it)
        currentFile = llist_item_data(it,odl_file_t,it);
    else
        currentFile = NULL;
}

static void clean_odl_files() {
    llist_iterator_t *it = llist_takeFirst(&odl_file_stack);
    odl_file_t *file = NULL;
    while(it) {
        odl_yypop_buffer_state();
        file = llist_item_data(it,odl_file_t,it);
        free(file->file_name);
        free(file);
        it = llist_takeFirst(&odl_file_stack);
    }
    currentFile = NULL;
}

bool odl_load_definitions(int socketfd, object_t *parent, const char *filename) {
    fatalError = 0;

    dm = object_datamodel(parent);
    pcb = datamodel_pcb(object_datamodel(parent));
    if (pcb != NULL) {
        connection_info_t *con = pcb_connection(pcb);
        pcb_name = connection_name(con);
    } else {
        pcb_name = "";
    }
    currentObject = parent;

    verify = false;
    out = stderr;

    char *fn = NULL;
    if (filename && *filename) {
        fn = strdup(filename);
        input_dir = dirname(fn);
        SAH_TRACEZ_INFO("odl","odl directory = %s",input_dir);
    } else {
        input_dir = NULL;
    }
    llist_initialize(&odl_file_stack);

    push_odl_file(filename);

    odl_yyin = fdopen(socketfd, "r");
    if (odl_yyin == NULL) {
      fprintf(out,"cannot open the parse file\n");
      SAH_TRACEZ_ERROR("odl","cannot open the parse file");
      goto error2;
    }

    if (yyparse() != 0 && fatalError != 0) {
        fprintf(out,"load failed\n");
        SAH_TRACEZ_ERROR("odl","load failed");
        goto error3;
    }

    if(fatalError) {
        fprintf(out,"Error loading object definition file\n");
        SAH_TRACEZ_ERROR("odl","Error loading object definition file");
        goto error3;
    }

    pop_odl_file();

#ifdef PCB_HELP_SUPPORT
    SAH_TRACEZ_INFO("odl","Clean up descriptions");
    string_cleanup(&description_line);
    string_cleanup(&description);
#endif

    if (!llist_isEmpty(&odl_file_stack)) {
        fprintf(out,"Error loading object definition file\n");
        SAH_TRACEZ_ERROR("odl","Error loading object definition file");
        goto error3;
    }

    if (!object_commit(parent)) {
        fprintf(out,"Commit Failed: 0x%.8X - %s\n",pcb_error,error_string(pcb_error));
        SAH_TRACEZ_ERROR("odl","Commit Failed: 0x%.8X - %s",pcb_error,error_string(pcb_error));
        goto error3;
    }

    fclose(odl_yyin);
    odl_yylex_destroy();

    llist_iterator_t *it = NULL;
    library_t *lib = NULL;
    SAH_TRACE_INFO("Initializing libraries");
    llist_for_each(it,&libraries) {
        lib = llist_item_data(it,library_t,it);

        if (lib->initialized) {
            SAH_TRACEZ_INFO("odl","Library already initialized");
            continue;
        }

        lib->initialized=true;
        // call the initializer
        pcb_plugin_initialize_handler pcb_plugin_initialize = dlsym(lib->handle, "pcb_plugin_initialize");
        if (pcb_plugin_initialize) {
            SAH_TRACEZ_INFO("odl","Initializing library");
            if (!pcb_plugin_initialize(pcb,pcb_argumentCount(pcb),pcb_arguments(pcb))) {
                SAH_TRACEZ_ERROR("odl","Failed to initialize library");
                goto error2;
            } else {
                SAH_TRACEZ_INFO("odl","Initializing done");
            }
        } else {
            // always call dlerror, otherwise the next dlsym will fail also
            const char *error = dlerror();
            SAH_TRACEZ_INFO("odl","No initialization function found for library - %s",error);
        }
    }
    SAH_TRACE_INFO("Initializing libraries done");

    free(fn);

    return true;


error3:
#ifdef PCB_HELP_SUPPORT
    SAH_TRACEZ_INFO("odl","Clean up descriptions");
    string_cleanup(&description_line);
    string_cleanup(&description);
#endif
    fclose(odl_yyin);
    odl_yylex_destroy();
    clean_odl_files();
error2:
    free(fn);
    return false;
}

bool odl_verify(int socketfd, const char *filename, variant_map_t *info,  bool *hasDefinition) {
    fatalError = 0;

    out = stderr;
    verify = true;

    variant_map_addListMove(info, "includes", NULL);
    variant_map_addMapMove(info, "types", NULL);
    variant_map_addMapMove(info, "locations", NULL);

    include_files = variant_map_da_findList(info, "includes");
    custom_types = variant_map_da_findMap(info, "types");
    file_locations = variant_map_da_findMap(info, "locations");

    definitions_available = hasDefinition;

    char *fn = NULL;
    if (filename && *filename) {
        fn = strdup(filename);
        input_dir = dirname(fn);
        SAH_TRACEZ_INFO("odl","odl directory = %s",input_dir);
    } else {
        input_dir = NULL;
    }
    llist_initialize(&odl_file_stack);

    push_odl_file(filename);

    odl_yyin = fdopen(socketfd, "r");
    if (odl_yyin == NULL) {
      fprintf(out,"cannot open the parse file\n");
      SAH_TRACEZ_ERROR("odl","cannot open the parse file");
      goto error1;
    }

#ifdef PCB_HELP_SUPPORT
    string_initialize(&description,0);
    string_initialize(&description_line,0);
#endif

    if (yyparse() != 0 && fatalError != 0) {
        fprintf(out,"load failed\n");
        SAH_TRACEZ_ERROR("odl","load failed");
        goto error3;
    }

    if(fatalError) {
        fprintf(out,"Error loading object definition file\n");
        SAH_TRACEZ_ERROR("odl","Error loading object definition file");
        goto error3;
    }

    pop_odl_file();

#ifdef PCB_HELP_SUPPORT
    string_cleanup(&description_line);
    string_cleanup(&description);
#endif

    if (!llist_isEmpty(&odl_file_stack)) {
        fprintf(out,"Error loading object definition file\n");
        SAH_TRACEZ_ERROR("odl","Error loading object definition file");
        goto error3;
    }

    free(fn);
    fclose(odl_yyin);
    odl_yylex_destroy();

    return true;

error3:
    fclose(odl_yyin);
    odl_yylex_destroy();
error1:
    free(fn);
    return false;
}

void odl_cleanup() {
    SAH_TRACEZ_INFO("odl","Cleanup pcb_serialize_odl");
    library_t *lib = NULL;
    llist_iterator_t *it = NULL;
    llist_for_each(it,&libraries) {
        lib = llist_item_data(it,library_t,it);
        // call the cleanup
        pcb_plugin_cleanup_handler pcb_plugin_cleanup = dlsym(lib->handle, "pcb_plugin_cleanup");
        if (pcb_plugin_cleanup) {
            SAH_TRACEZ_INFO("cleanup","Calling pcb_plugin_cleanup");
            pcb_plugin_cleanup();
        } else {
          // always call dlerror, otherwise the next dlsym will fail also
          const char *error = dlerror();
          SAH_TRACEZ_INFO("cleanup","No cleanup function found - %s",error);
        }
     }
}

void odl_unregister() {
    SAH_TRACEZ_INFO("odl","Unregistering pcb_serialize_odl");
    // remove all libraries
    library_t *lib = NULL;
    llist_iterator_t *it = llist_takeFirst(&libraries);
    while(it) {
        lib = llist_item_data(it,library_t,it);
        pcb_plugin_exit_handler pcb_plugin_exit = dlsym(lib->handle, "pcb_plugin_exit");
        if (pcb_plugin_exit) {
            pcb_plugin_exit();
        } else {
          // always call dlerror, otherwise the next dlsym will fail also
          const char *error = dlerror();
          SAH_TRACEZ_INFO("odl","No exit function found - %s",error);
        }
        dlclose(lib->handle);
        free(lib->filename);
        free(lib);
        it = llist_takeFirst(&libraries);
    }
    llist_cleanup(&libraries);
}

int odl_yywrap() {
    return 1;
}

void libpcb_serialize_odl_register() {
    SAH_TRACEZ_INFO("odl","Initializing pcb_serialize_odl");
    if (!llist_initialize(&libraries)) {
        SAH_TRACEZ_ERROR("odl","Failed to initialize odl serializer");
        return;
    }

    serialize_handlers_t *handlers = (serialize_handlers_t *)calloc(1,sizeof(serialize_handlers_t));
    if (!handlers) {
        return;
    }

    handlers->save = odl_save_datamodel;
    handlers->load = odl_load_definitions;
    handlers->verify = odl_verify;
    handlers->cleanup = odl_cleanup;
    handlers->unregister = odl_unregister;

    serialization_addSerializer(handlers,SERIALIZE_FORMAT(serialize_format_odl,0,0));
    SAH_TRACEZ_INFO("odl", "loaded odl serializer plugin");
}
