/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <errno.h>
#include <inttypes.h>
#include <string.h>

#include <debug/sahtrace.h>

#include <pcb/common.h>
#include <pcb/utils.h>
#include <pcb/core.h>

#include <components.h>

#ifdef CONFIG_PCB_ACL_USERMNGT
#include <usermngt/usermngt.h>
#endif

static uint32_t level = 0;

static void odl_save_add_identation(FILE* out) {
    uint32_t i = 0;
    for(i = 0; i < level + 1; i++) {
        fprintf(out, "    ");
    }
}

#ifdef CONFIG_PCB_ACL_USERMNGT
static void odl_save_acl(const llist_t* acllist, FILE* out) {
    ACL_t* acl = NULL;
    const char* sep = " ";
    const usermngt_user_t* user = NULL;
    const usermngt_group_t* group = NULL;
    bool has_persistent_settings = false;
    // count persistent settings
    llist_t acllist_save;
    llist_initialize(&acllist_save);
    acl_for_each(acl, acllist) {
        if(acl_flags(acl) & acl_group_id) {
            group = usermngt_groupFindByID(acl_id(acl));
            if(group) {
                if(usermngt_groupName(group) && (strncmp(usermngt_groupName(group), "__", 2) == 0)) {
                    SAH_TRACEZ_INFO("odl", "Non persistent group %s", usermngt_groupName(group));
                    continue;
                } else {
                    has_persistent_settings = true;
                    break;
                }
            }
            continue;
        } else {
            user = usermngt_userFindByID(acl_id(acl));
            if(user) {
                if(usermngt_userName(user) && (strncmp(usermngt_userName(user), "__", 2) == 0)) {
                    SAH_TRACEZ_INFO("odl", "Non persistent user %s", usermngt_userName(user));
                    continue;
                } else {
                    has_persistent_settings = true;
                    break;
                }
            }
            continue;
        }
    }
    if(!has_persistent_settings) {
        return;
    }

    odl_save_add_identation(out);
    fprintf(out, "acl {\n");

    level++;
    acl_for_each(acl, acllist) {
        odl_save_add_identation(out);
        sep = " ";
        group = NULL;
        user = NULL;

        SAH_TRACEZ_INFO("odl", "Saving acl for id = %d", acl_id(acl));

        if(acl_id(acl) == UINT32_MAX) {
            fprintf(out, "world");
        } else if(acl_flags(acl) & acl_group_id) {
            group = usermngt_groupFindByID(acl_id(acl));
            if(group) {
                if(usermngt_groupName(group) && (strncmp(usermngt_groupName(group), "__", 2) == 0)) {
                    SAH_TRACEZ_INFO("odl", "Non persistent group %s", usermngt_groupName(group));
                    continue;
                }
                SAH_TRACEZ_INFO("odl", "Saving acl entry for group %s", usermngt_groupName(group));
                fprintf(out, "group \"%s\"", usermngt_groupName(group));
            } else {
                SAH_TRACEZ_ERROR("odl", "No group found with id %d", acl_id(acl));
                fprintf(out, "\n");
                continue;
            }
        } else {
            user = usermngt_userFindByID(acl_id(acl));
            if(user) {
                if(usermngt_userName(user) && (strncmp(usermngt_userName(user), "__", 2) == 0)) {
                    SAH_TRACEZ_INFO("odl", "Non persistent user %s", usermngt_userName(user));
                    continue;
                }
                SAH_TRACEZ_INFO("odl", "Saving acl entry for user %s", usermngt_userName(user));
                fprintf(out, "user \"%s\"", usermngt_userName(user));
            } else {
                SAH_TRACEZ_ERROR("odl", "No user found with id %d", acl_id(acl));
                usermngt_reload();
                user = usermngt_userFindByID(acl_id(acl));
                if(user) {
                    if(usermngt_userName(user) && (strncmp(usermngt_userName(user), "__", 2) == 0)) {
                        SAH_TRACEZ_INFO("odl", "Non persistent user %s", usermngt_userName(user));
                        continue;
                    }
                    SAH_TRACEZ_INFO("odl", "Saving acl entry for user %s", usermngt_userName(user));
                    fprintf(out, "user \"%s\"", usermngt_userName(user));
                } else {
                    SAH_TRACEZ_ERROR("odl", "No user found with id %d for the second time", acl_id(acl));
                    fprintf(out, "world none;\n");
                    continue;
                }
            }
        }

        if(acl_flags(acl) & acl_read) {
            fprintf(out, "%sread", sep);
            sep = ",";
        }
        if(acl_flags(acl) & acl_write) {
            fprintf(out, "%swrite", sep);
            sep = ",";
        }
        if(acl_flags(acl) & acl_execute) {
            fprintf(out, "%sexecute", sep);
            sep = ",";
        }
        if((acl_flags(acl) & (acl_read | acl_write | acl_execute)) == 0) {
            fprintf(out, "%snone", sep);
        }
        fprintf(out, ";\n");
    }
    level--;

    odl_save_add_identation(out);
    fprintf(out, "}\n");
}
#else
#define odl_save_acl(acllist, out)
#endif

static void odl_save_mibs(const string_list_t* mibs, FILE* out) {
    char** mib = NULL;
    string_list_for_each_char(mib, mibs) {
        odl_save_add_identation(out);
        fprintf(out, "extend using mib %s;\n", *mib);
    }
}

static void odl_save_parent_tree(object_t* object, object_t* root, FILE* out) {
    if(object != root) {
        SAH_TRACEZ_INFO("odl", "Tree up: object = %s", object_name(object, path_attr_key_notation));
        odl_save_parent_tree(object_parent(object), root, out);
    }

    if(object == root) {
        return;
    }

    if(object_isTemplate(object)) {
        return;
    }

    SAH_TRACEZ_INFO("odl", "Saving object: %s", object_name(object, path_attr_key_notation));
    if(object_isInstance(object)) {
        odl_save_add_identation(out);
        fprintf(out, "instance of %s(%s,\"%s\") {\n", object_name(object_parent(object), path_attr_key_notation), object_name(object, path_attr_default), object_name(object, path_attr_key_notation));
    } else {
        odl_save_add_identation(out);
        fprintf(out, "object %s {\n", object_name(object, path_attr_default));
    }

    level++;
    if(object_hasAcl(object)) {
        odl_save_acl(object_getACL(object), out);
    }

    return;
}

static void odl_save_parameter_value(parameter_t* parameter, FILE* out) {
    switch(parameter_type(parameter)) {
    case parameter_type_unknown:
    case parameter_type_string:
    case parameter_type_reference: {
        string_t data;
        string_initialize(&data, 0);
        string_copy(&data, variant_da_string(parameter_getValue(parameter)));
        string_replaceChar(&data, "\\", "\\\\");
        string_replaceChar(&data, "\"", "\\\"");
        string_replaceChar(&data, "$", "\\$");
        string_replaceChar(&data, "(", "\\(");
        string_replaceChar(&data, ")", "\\)");
        fprintf(out, "\"%s\"", string_isEmpty(&data) ? "" : string_buffer(&data));
        string_cleanup(&data);
    }
    break;
    case parameter_type_date_time: {
        char* value = variant_char(parameter_getValue(parameter));
        fprintf(out, "\"%s\"", value);
        free(value);
    }
    break;
    case parameter_type_bool:
        fprintf(out, "%s", variant_bool(parameter_getValue(parameter)) ? "true" : "false");
        break;
    case parameter_type_int8:
    case parameter_type_int16:
    case parameter_type_int32:
        fprintf(out, "%d", variant_int32(parameter_getValue(parameter)));
        break;
    case parameter_type_int64:
        fprintf(out, "%" PRId64, variant_int64(parameter_getValue(parameter)));
        break;
    case parameter_type_uint8:
    case parameter_type_uint16:
    case parameter_type_uint32:
        fprintf(out, "%u", variant_uint32(parameter_getValue(parameter)));
        break;
    case parameter_type_uint64:
        fprintf(out, "%" PRIu64, variant_uint64(parameter_getValue(parameter)));
        break;
    }
}

static void odl_save_parameter(parameter_t* parameter, FILE* out) {
    odl_save_add_identation(out);
    if(parameter_hasAcl(parameter)) {
        fprintf(out, "parameter %s {\n", parameter_name(parameter));

        level++;
        odl_save_acl(parameter_getACL(parameter), out);

        // write default value
        odl_save_add_identation(out);
        fprintf(out, "default ");
        odl_save_parameter_value(parameter, out);
        fprintf(out, ";\n");
        level--;

        odl_save_add_identation(out);
        fprintf(out, "}\n");
    } else {
        fprintf(out, "parameter %s=", parameter_name(parameter));
        odl_save_parameter_value(parameter, out);
        fprintf(out, ";\n");
    }
}

static void odl_save(object_t* object, uint32_t depth, FILE* out) {
    bool hasBody = false;

    pcb_t* pcb = datamodel_pcb(object_datamodel(object));

    SAH_TRACEZ_INFO("odl", "Saving object: %s (%p)", object_name(object, path_attr_key_notation), object);
    if(!object_isTemplate(object)) {
        // create object
        if(object_isInstance(object)) {
            const char* type = "";
            if(pcb_upcIsEnabled(pcb)) {
                switch(object_attributes(object) & (object_attr_persistent | object_attr_upc | object_attr_upc_usersetting)) {
                case object_attr_persistent | object_attr_upc | object_attr_upc_usersetting:
                    type = "usersetting ";
                    break;
                case object_attr_persistent | object_attr_upc:
                    type = "upc ";
                    break;
                case object_attr_persistent | object_attr_upc_usersetting:
                case object_attr_persistent:
                    type = "persistent ";
                    break;
                default:
                    type = "";
                    break;
                }
            } else {
                switch(object_attributes(object) & (object_attr_persistent)) {
                case object_attr_persistent:
                    type = "persistent ";
                    break;
                default:
                    type = "";
                    break;
                }
            }
            odl_save_add_identation(out);
            fprintf(out, "%sinstance of %s(%s,\"%s\")", type, object_name(object_parent(object), path_attr_key_notation), object_name(object, path_attr_default), object_name(object, path_attr_key_notation));
        } else {
            odl_save_add_identation(out);
            fprintf(out, "object %s", object_name(object, path_attr_default));
        }

        if(object_hasAcl(object)) {
            fprintf(out, " {\n");
            hasBody = true;
            level++;
            odl_save_acl(object_getACL(object), out);
        }

        const string_list_t* mibs = object_mibs(object);
        if(!string_list_isEmpty(mibs)) {
            if(!hasBody) {
                fprintf(out, " {\n");
                hasBody = true;
                level++;
            }
            odl_save_mibs(mibs, out);
        }

        // save persistent parameters
        parameter_t* parameter = NULL;
        object_for_each_parameter(parameter, object) {
            // do not store mapped parameters
            if(parameter_destination(parameter)) {
                continue;
            }

            // only store persistent parameters
            if(!(parameter_attributes(parameter) & parameter_attr_persistent)) {
                continue;
            }

            if(!hasBody) {
                fprintf(out, " {\n");
                level++;
                hasBody = true;
            }

            odl_save_parameter(parameter, out);
        }

        // max depth reached, stop here
        if(depth == 0) {
            goto close_object;
        }

        // save persistent childs
        object_t* child = NULL;
        object_for_each_child(child, object) {
            // only store persistent objects
            if(!(object_attributes(child) & object_attr_persistent) && !object_isTemplate(child)) {
                continue;
            }

            // skip empty template objects
            if(object_isTemplate(child) && !object_hasInstances(child)
               && string_list_isEmpty(object_getDeletedInstances(child, deleted_instance_upc))
               && string_list_isEmpty(object_getDeletedInstances(child, deleted_instance_usersetting))) {
                continue;
            }

            if(!hasBody) {
                fprintf(out, " {\n");
                level++;
                hasBody = true;
            }

            odl_save(child, (depth == UINT32_MAX) ? depth : depth - 1, out);
        }
    } else {
        // save persistent childs
        object_t* child = NULL;
        object_for_each_instance(child, object) {
            // only store persistent objects
            if(!(object_attributes(child) & object_attr_persistent)) {
                continue;
            }

            odl_save(child, (depth == UINT32_MAX) ? depth : depth - 1, out);
        }

        if(pcb_upcIsEnabled(pcb)) {
            // save deleted persistent children
            int i = 0;
            const char* type = "";
            const string_list_t* list = NULL;
            string_list_iterator_t* iter = NULL;
            for(i = 0; i < deleted_instance_max; i++) {
                switch(i) {
                case deleted_instance_persistent:
                    type = "persistent ";
                    break;
                case deleted_instance_upc:
                    type = "upc ";
                    break;
                case deleted_instance_usersetting:
                    type = "usersetting ";
                    break;
                default:
                    type = "";
                    break;
                }
                list = object_getDeletedInstances(object, i);

                string_list_for_each(iter, list) {
                    odl_save_add_identation(out);
                    fprintf(out, "deleted %sinstance of %s(\"%s\");\n", type, object_name(object, path_attr_key_notation), string_buffer(string_list_iterator_data(iter)));
                }
            }
        }
    }

close_object:
    if(hasBody) {
        level--;
        odl_save_add_identation(out);
        fprintf(out, "}\n");
    } else {
        if(!object_isTemplate(object)) {
            fprintf(out, ";\n");
        }
    }
}

static void odl_save_close_parent_tree(FILE* out) {
    while(level) {
        odl_save_add_identation(out);
        fprintf(out, "}\n");
        level--;
    }
}

bool odl_save_datamodel(int fd, object_t* object, uint32_t depth, const char* filename) {
    FILE* out = fdopen(fd, "w");

    if(!out) {
        SAH_TRACEZ_ERROR("odl", "failed to create stream for output file");
        pcb_error = errno;
        return false;
    }

    SAH_TRACEZ_INFO("odl", "Saving data model to %s (object = %p)", filename, object);
    level = 0;

    object_t* root = object_root(object);
    object_t* parent = object_parent(object);

    SAH_TRACEZ_INFO("odl", "Write datamodel");
    fprintf(out, "datamodel {\n");

    // build odl file up to given object
    if(parent) {
        SAH_TRACEZ_INFO("odl", "Build parent tree");
        odl_save_parent_tree(parent, root, out);
    }

    if(object == root) {
        SAH_TRACEZ_INFO("odl", "Write childeren of root object");
        object_t* child = NULL;
        object_for_each_child(child, object) {
            // only store persistent objects
            if(!(object_attributes(child) & object_attr_persistent)) {
                continue;
            }

            odl_save(child, (depth == UINT32_MAX) ? depth : depth - 1, out);
        }
    } else {
        // save the requested objects
        SAH_TRACEZ_INFO("odl", "Save objects");
        odl_save(object, depth, out);
    }

    // close objects
    SAH_TRACEZ_INFO("odl", "Close objects");
    odl_save_close_parent_tree(out);

    // close datamodel
    SAH_TRACEZ_INFO("odl", "Close datamodel");
    fprintf(out, "}\n");

    // implementation according to http://lwn.net/Articles/457667/
    /*
     * After the fwrite call returns, the data is in libc's stdio
     * buffer (still in the application's address space).  So, the
     * next thing we want to do is flush that buffer.
     */
    if(fflush(out) != 0) {
        SAH_TRACEZ_ERROR("odl", "fflush failed: %d - %s", errno, error_string(errno));
    }
    /*
     * Now the data is in the kernel's page cache.  The next steps
     * flush the page cache for this file to disk.
     */
    if(fsync(fd) < 0) {
        SAH_TRACEZ_ERROR("odl", "fsync failed: %d - %s", errno, error_string(errno));
    }

    if(fclose(out) < 0) {
        SAH_TRACEZ_ERROR("odl", "fclose failed: %d - %s", errno, error_string(errno));
    }

    return true;
}
