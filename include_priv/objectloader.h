/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef OBJECTLOADER_H
#define OBJECTLOADER_H


/**
   @file
   @brief
   Header file for the pcb objectloader data files
 */

/**
   @ingroup pcb_plugin
   @defgroup pcb_plugin_objectloader Object Loader
   @{

   @brief
   This file documents the objectloader file format

   @details

   @section pcb_plugin_objectloader_gramar_comments Comments
   The objectloader file supports C-like comments e.g. / * ... * / and //

   @section pcb_plugin_objectloader_gramar_objects Object syntax:

   <b> Object syntax:\n</b>
   attributes object MyObjectName [x] { \n
   ...\n
   }\n

   <b>Explanation:\n</b>
   \e attributes: optional, one or more entries of the folowing options, seperated by spaces:
   @li read-only: a read only objects disable writes to the object parameters
   @li template:
   @li accept-parameters:
   @li dispatch:

   \e object: mandatory, this is the keyword defining that we're creating an object

   \e MyObjectName: mandatory, this string is the objectname, which can be freely chosen, the only limitations are:
   @li the name should contain only letters and numbers, _ or -, all other special characters are not allowed
   @li each name should at least contain one letter (so numeric-only values are not allowed)
   The objectname should also contain the parent path defining the location of the parent object. If no parent path
   is given, the object will be created in the root of the datamodel. The only exception to this rule is when an
   object is defined within the brackets of another object, which is then used as the parent object.

   \e [x]: optional, to indicate that this object is a multi-instance object. x is the maximum number of instances of
   this object. If no x is given e.g. [], there is no limit on the number of children,

   \e { ... }: mandatory, this is the object body, between these brackets the parameters, functions and constraints
   of the object are defined, it is also possible to define sub-objects between these brackets. In case of using
   this objectloader file for a proxy the destinationurl and translation can also be mentioned here.

   \e Examples:
   @li object storage {...}: The single-instance object called 'storage'
   @li read-only accept-parameters object partition [] {...}: The multi-instance object called 'partition' with
   the properties 'read-only' and 'accept-parameters' and no limit on the number of instances.

   @section pcb_plugin_objectloader_gramar_objectbody_functions Functions syntax:

   <b>function syntax:\n</b>
   callback with FunctionName;\n

   <b>Explanation:\n</b>
   Callbacks are used for calling specific c-functions when an event (read/write/add/delete) occurs.\n

   \e callback: mandatory, this keyword defines the type of callback: read, write, add, delete\n

   \e with: mandatory, this keyword indicates that this line defines a callback function\n

   \e FunctionName: mandatory, the function name used in the c-code that contains the callback code\n

   @section pcb_plugin_objectloader_gramar_objectbody_custom Object custom constraints syntax:\n
   Custom constraints are used to set a specific validation function implemented in c.\n

   <b>custom contraint syntax:\n</b>
   constraint custom FunctionName;\n

   <b>Explanation:\n</b>

   \e constraint custom: mandatory, this keyword defines that this is a custom constraint\n

   \e FunctionName: mandatory, thie function name used in c-code that contains validation functionality\n

   @section pcb_plugin_objectloader_gramar_objectbody_destinationurl Object destinationurl syntax:

   <b>destinationurl syntax:\n</b>
   destination protocol path;\n
   destination protocol path: number;\n

   <b>Explanation:\n</b>

   \e destination: mandatory, keyword indication this is a destinationurl\n

   \e protocol: mandatory, IPC or TCP\n

   \e path: mandatory, the target path\n

   \e number: optional\n

   @section pcb_plugin_objectloader_gramar_objectbody_translate Object translate syntax:

   <b>translate syntax:\n</b>
   translate TranslationPath {\n
   indexpath to ObjectOrPath;\n
   keypath to ObjectOrPath;\n
   }\n

   <b>Explanation:\n</b>

   \e translate: mandatory, keyword indicating this is a translation\n

   \e TranslationPath: mandatory, the translation path\n

   \e indexpath to: mandatory, keywords indicating this is the indexpath\n

   \e keypath to: mandatory, keywords indicating this is the keypath \n

   Both indexpath and keypath are mandatory!\n

   \e ObjectOrPath: mandatory, the object or path you are translating\n

   @section pcb_plugin_objectloader_gramar_objectbody_parameters Object parameter syntax:

   <b>parameter syntax:\n</b>
   attributes paramtype ParamName = DefaultValue;\n
   attributes paramtype ParamName {\n
   ...\n
   }\n

   <b>Explanation:\n</b>

   \e attributes: optional, these are the parameter attributes:\n
   @li read-only:
   @li persistent:
   @li mandatory:
   @li template-only:


   \e paramtype: mandatory, this is the keyword indicating the parameter type:\n
   @li string
   @li int8, int16, int32
   @li uint8, uint16, uint32
   @li bool
   @li datetime
   @li reference

   \e ParamName: mandatory, this is the parameter name, some restrictions apply:\n
   @li the name should contain only letters and numbers, _ or -, all other special characters are not allowed
   @li each name should at least contain one letter (so numeric-only values are not allowed)

   \e DefaultValue: optional, this is the default value of the parameter, long strings containing spaces must be enclosed
   within quotes e.g."my default string".\n

   { ... }: optional, this parameter body can be used to define extra functions constraints or default value\n

   @section pcb_plugin_objectloader_gramar_objectbody_parameter_function Parameter function syntax:

   <b>parameter function syntax:\n</b>
   callback with FunctionName;\n

   <b>Explanation:\n</b>

   \e callback: mandatory, this keyword defines the callback function type, 'read' or 'write'\n

   \e with: mandatory, this keyword indicates that this line defines a callback function\n

   \e FunctionName: mandatory, the function name used in the c-code that contains the callback code\n

   @section pcb_plugin_objectloader_gramar_objectbody_parameter_constraint Parameter constraint syntax:

   <b>parameter constraint syntax:\n</b>
   constraint minvalue Number;\n
   constraint maxvalue Number;\n
   constraint range [Number, Number];\n
   constraint enum [Enumeration];\n
   constraint custom FunctionName;\n

   <b>Explanation:\n</b>

   \e constraint: mandatory, this keyword defines that this is a constraint definition\n

   \e minvalue: mandatory for defining a minimal value constraint\n

   \e maxvalue: mandatory for defining a maximum value constraint\n

   \e range: mandatory for defining a range constraint\n

   \e enum:  mandatory for defining an enum constraint\n

   \e Number: mandatory, the numeric values for this constraint\n

   \e Enumeration: mandatory, a list of possible values e.g. Value1,"Value 2",...\n

   \e FunctionName: mandatory, thie function name used in c-code that contains validation functionality\n


   @section pcb_plugin_objectloader_gramar_objectbody_parameter_defaultvalue Parameter default value syntax:

   <b>parameter default value syntax:\n</b>
   default MyValue;\n

   <b>Explanation:\n</b>

   \e default: mandatory, keyword indicating this is a default value\n

   \e MyValue: this is the default value of the parameter, long strings containing spaces must be enclosed
   within quotes e.g."my default string".\n


   @section pcb_plugin_objectloader_examples Objectloader examples:
 */

enum attributes {
    attr_none             = 0x00000000,
    attr_readonly         = 0x00000001,
    attr_persistent       = 0x00000002,
    attr_templateonly     = 0x00000008,
    attr_template         = 0x00000010,
    attr_acceptparameters = 0x00000020,
    attr_private          = 0x00000040,
    attr_in               = 0x00000100,
    attr_out              = 0x00000200,
    attr_not_persistent   = 0x00000400,
    attr_volatile         = 0x00000800,
    attr_message          = 0x00001000,
    attr_mandatory        = 0x00002000,
    attr_upc              = 0x00004000,
    attr_not_upc          = 0x00008000,
    attr_usersetting      = 0x00010000,
    attr_not_usersetting  = 0x00020000,
    attr_upc_overwrite    = 0x00040000,
    attr_key              = 0x00080000,
    attr_async            = 0x00100000,
};

enum types {
    type_unknown = 0,
    type_void = 0,
    type_string,
    type_int8,
    type_int16,
    type_int32,
    type_int64,
    type_uint8,
    type_uint16,
    type_uint32,
    type_uint64,
    type_bool,
    type_date_time,
    type_reference,
    type_list,
    type_file_descriptor,
    type_byte_array,
    type_variant,
    type_double,
};

enum keywords
{
    keyword_object,
    keyword_acl,
    keyword_read,
    keyword_user,
    keyword_write,
    keyword_group,
    keyword_world,
    keyword_add,
    keyword_delete,
    keyword_default,
    keyword_with,
    keyword_using,
    keyword_constraint,
    keyword_minvalue,
    keyword_maxvalue,
    keyword_range,
    keyword_enum,
    keyword_custom,
    keyword_translate,
    keyword_indexpath,
    keyword_keypath,
    keyword_to,
    keyword_include,
    keyword_define,
    keyword_datamodel,
    keyword_instance,
    keyword_of,
    keyword_destination,
    keyword_from,
    keyword_parameter,
    keyword_function,
    keyword_recursive,
    keyword_accept,
    keyword_drop,
    keyword_rename,
    keyword_request,
    keyword_cast,
    keyword_extends,
    keyword_mib,
    keyword_extend,
    keyword_mib_add,
    keyword_mib_del,
    keyword_counted,
    keyword_prewrite,
    keyword_deleted,
};

enum protocols
{
    protocol_ipc,
    protocol_tcp,
};

#define INITIAL_TEXT_SIZE 1023

void yy_state_set(int new_state);

/**
   @}
 */

#endif
