# pcb-ser-odl

## Summary

This library provides the code to serialize and deserialize
the data model to and from an ODL file.

## Description

Each PCB plug-in has an in-memory representation of the exported
data model. When saving the data model settings,
that internal representation needs to be serialized to a format
suitable to be stored into a regular file.
The data model definitions, default and saved values are also
loaded from a regular file and need to be deserialized to update
the internal data structure.

The ODL serializer is used as main serializer for communication
between PCB plug-in's and the data model related files.
These files carry the extension `.odl`.
ODL = Object Description Language.
