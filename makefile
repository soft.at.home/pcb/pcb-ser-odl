-include $(CONFIGDIR)/components.config

export INSTALL ?= install
export PKG_CONFIG_LIBDIR ?= /usr/lib/pkgconfig
export BINDIR ?= /usr/bin
export LIBDIR ?= /usr/lib
export SLIBDIR ?= /usr/lib
export LUALIBDIR ?= /usr/lib/lua
export INCLUDEDIR ?= /usr/include
export INITDIR ?= /etc/init.d
export ACLDIR ?= /etc/acl
export DOCDIR ?= $(D)/usr/share/doc/pcb-ser-odl
export PROCMONDIR ?= /usr/lib/processmonitor/scripts
export RESETDIR ?= /etc/reset
export MACHINE ?= $(shell $(CC) -dumpmachine)

export COMPONENT = pcb-ser-odl

compile:
	$(MAKE) -C src all

clean:
	$(MAKE) -C src clean

install:
	$(INSTALL) -D -p -m 0755 src/libpcb_serialize_odl.so $(D)$(LIBDIR)/pcb/libpcb_serialize_odl.so

.PHONY: compile clean install
